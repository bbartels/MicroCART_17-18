function momentOfInertia = getMomentOfInertia( fullFilePath, massUsed_grams )
%This function will calculate a single moment of inertia value, given a
%file location to a csv file, along with the mass value used on the mass
%handle as a torque. (Note, there's no need to have to consider the mass of
%the handle, which is 5.0 grams, since this function takes care of that)

    %fileLocation = 'C:\Users\Tara\Desktop\Project Documents\Current Project Documents\EE 491\Data\Physics Department Measurements\Test\';
    %fileName = 'Test_1.csv';

    %Mass used in grams
    %mass_grams = 200.1;
    massHandle_grams = 5.0;
    totalMass_grams = massUsed_grams + massHandle_grams;
    totalMass_kg = totalMass_grams / 1000.0;

    %Define a cut-off start angle in radians
    angleStart_cutoff = 0.1;

    %Radius of the knob where the string was pulling from (in meters)
    radius = 0.04445;   %(4.445 centimeters - or 1.75 inches)

    %Acceleration constant due to gravity (in meters per second squared)
    g = 9.80665;

    %Define a cut-off "padding" to stop taking data before reaching the end of
    %the data recorded in the file
    %For example, if dataEndPadding is 10, we will "cut-off" our data when we
    %get to 10 data points from the very end of the data run
    dataEndPadding = 5;

    %Define columns of the table
    timeCol = 1;
    angleCol = 2;
    angularVelCol = 3;
    %angularAccelCol = 4;

    %Create full file path
    %fullFilePath = [fileLocation, fileName];

    %Bring in Data
    dataTable = readtable(fullFilePath);

    %Extract the time, angle (in radians), and angular velocity (in radians per
    %second) arrays from the data table
    timeArray = dataTable{:, timeCol};
    angleArray = dataTable{:, angleCol};
    angularVelArray = dataTable{:, angularVelCol};

    %Find the first positive angle value
    i_start = find(angleArray > angleStart_cutoff, 1);

    %Get the start time and start angular position, based on this start index
    time_start = timeArray(i_start);
    angle_start = angleArray(i_start);

    %Find the initial angular velocity, which is the corresponding angular 
    %velocity value at this start index (in radians per second)
    w_o = angularVelArray(i_start);

    %Determine the end time of our data collection, so first get the length of
    %the time array
    timeArray_len = length(timeArray);

    %Get the last index based on the length of the data and the "padding"
    %amount defined at the beginning
    i_end = timeArray_len - dataEndPadding;

    %Get the end time and end angular position in radians
    time_end = timeArray(i_end);
    angle_end = angleArray(i_end);

    %Get the difference in time and angular position (in radians)
    angle_diff = angle_end - angle_start;
    time_diff = time_end - time_start;

    %Using this expression, where alpha is the constant angular acceleration:
    %   theta_diff = w_o*t_diff + (1/2)*alpha*(t_diff^2)
    %We can solve for alpha to get
    %   (theta_diff - w_o*t_diff) = (1/2)*alpha*(t_diff^2)
    %   2*(theta_diff - w_o*t_diff) / (t_diff^2) = alpha

    %Thus the angular acceleration in radians per second-squared, denoted by 
    %alpha can be found like this:
    angularAccel = 2*(angle_diff - w_o*time_diff) / (time_diff * time_diff);

    %This constant angular acceleration should be due to the torque caused by
    %the gravitational force caused by our falling mass (m*g*r)
    torque_gravity = totalMass_kg * g * radius;

    %The moment of inertia would be this torque divided by the constant angular
    %acceleration value (in kilograms meters-squared)
    momentOfInertia = torque_gravity / angularAccel;


end

