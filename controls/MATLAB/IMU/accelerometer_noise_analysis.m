close all; clear all; clc; 

filename = 'imu.csv';
data = load_IMU_data_from_csv( filename ) ; 

t = data.t; 
ax = data.ax; 
ay = data.ay; 
az = data.az; 
gx = data.gx; 
gy = data.gy; 
gz = data.gz; 

% analyze the noise (deviation from mean) on each axis and compare the
% emperical distribution to one generated from a quantized Gaussian
% distrubtuion with the same variance. 
quantized_noise_gauss_anal( ax , 'Accelerometer' , 'x-axis' , 'gs' )
quantized_noise_gauss_anal( ay , 'Accelerometer' , 'y-axis', 'gs' )
quantized_noise_gauss_anal( az , 'Accelerometer' , 'z-axis', 'gs' )


quantized_noise_gauss_anal( gx , 'Gyroscope' , 'x-axis' , 'rad/s')
quantized_noise_gauss_anal( gy , 'Gyroscope' , 'y-axis' , 'rad/s' )
quantized_noise_gauss_anal( gz , 'Gyroscope' , 'z-axis' , 'rad/s' )