function varargout = quantized_noise_gauss_anal( a , sensor_name, axis , units )
% quantized_noise_gauss_anal analysis for quantized sensor data 
%
%SYNTAX
% quantized_noise_gauss_anal(a , sensor_name, axis , units )
% 
%DESCRIPTION
% ...to be written...
%
%
%Inputs:
%           a - vector of a single axis accelerometer reading 
% sensor_name - string specifying the axis label 
%        axis - string specifying the axis label 
%       units - string specifying the physical units on that axis 
%
%Options:
% none 
%
%Outputs:
% none 
%
%EXAMPLES
%
%NOTES
% This function is only really meaningful for a static (not moving) test. 
%
%AUTHORS
% Matt Rich - m87rich@iastate.edu
%
%DEVELOPERS
%

%
%DEVELOPMENT NOTES
%
% dates are in m-d-y format 
%
% Initial bare bones function. 
% - Matt Rich 11-15-2016
%
% Change funcdtion name and code to not specify what sensor it is for so it
% can be used for any sensor producing quantized output
% - Matt Rich 11-16-2016
%

mu = mean(a); 
v = var(a); 
sigma = sqrt(v); 

ea = a - mu; 

mue = mean(ea); %calculate the mean of the error from mean


figure; 

bins = unique(ea) ; 
dbins = bins(2)-bins(1); % assuming the quantization levels consistent 

bins_right = bins + dbins/2; 
bins_left = bins - dbins/2; 
cgauss_right = normcdf(bins_right,mue,sigma); 
cgauss_left = normcdf(bins_left,mue,sigma); 

emp_dist = length(ea)*(cgauss_right-cgauss_left); 
stairs(bins_left,emp_dist,'r','LineWidth',1) ; 
xlabel(['Error From Mean (',units,')']); 
ylabel('Occurences'); 
grid; 
hold on; 
h = hist(ea,bins); 
stem(bins,h); 
legend(['Quantized Gaussian Noise: N(',num2str(mue),',',num2str(v),')'],'Emperical Distribution'); 
title([sensor_name,' deviation from mean on ',axis]); 


end

