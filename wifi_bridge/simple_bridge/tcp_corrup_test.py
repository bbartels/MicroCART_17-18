import socket
import time
import csv

TCP_IP = "192.168.4.1"
TCP_PORT = 8080

# sock.bind(('', UDP_PORT))

send_size = 350
message = bytes(i % 256 for i in range(send_size))
times_full = []
times_network = []
dropped = True
response = bytes("initial", 'ASCII')
recvd_data = []

n_matched = 0

for i in range(20000):
    if dropped:
        attempts = 0
        while attempts < 5:
            print("Trying to connect")
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(2)
            try:
                sock.connect((TCP_IP, TCP_PORT))
                dropped = False
                break
            except:
                attempts += 1
        if dropped:
            print("Failed to connect")
            break
        print("connected")
    send_msg = message

    sock.send(send_msg)
    try:
        # while len(recvd_data) < send_size+4:
        response = sock.recv(1024)
        recvd_data.extend(response)
    except:
        print("timed out")
        dropped = True
        sock.close()

    if len(recvd_data) >= send_size or dropped:
        response = bytes(recvd_data[0:send_size])
        if response != message:
            print("Don't match")
            print(":".join("{:02x}".format(c) for c in message))
            print(":".join("{:02x}".format(c) for c in response))
        else:
            n_matched += 1
        recvd_data = recvd_data[send_size:]

        # written = int.from_bytes(recvd_data[0:4], byteorder='little')
        # recvd_data = recvd_data[4:]
        # print("ESP said wrote " + str(written))

        # print(":".join("{:02x}".format(c) for c in response))
        if dropped:
            recvd_data = []
            print("")
            # break
    time.sleep(0.01)
    if i % 1000 == 0:
        print(i)

print("Successfully received " + str(n_matched))


