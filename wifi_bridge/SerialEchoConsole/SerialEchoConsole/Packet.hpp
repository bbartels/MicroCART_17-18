#pragma once

#include <vector>
#include <string>
#include <stdint.h>

struct Packet
{
	Packet();
	Packet(uint8_t type, uint8_t subtype, uint16_t id,
		const std::vector<unsigned char>& payload);

	operator bool() const;

	std::string toString() const;

	std::vector<unsigned char> toVector() const;

	uint8_t type, subtype;
	uint16_t id;
	//uint16_t length;
	std::vector<unsigned char> payload;
	uint8_t checksum;
	bool isValid;
};

std::vector<Packet> parsePacket(const std::vector<unsigned char>&);

Packet findPacket(std::vector<unsigned char>&);