#include "stdafx.h"
#include "SerialPort.h"
#include "Packet.hpp"
#include <conio.h>
#include <thread>

#include <iostream>
#include <iomanip>
#include <string>
#include <chrono>

#define SEND_SIZE 512
#define DELAY 10


void printHex(std::vector<unsigned char> data) {
	for (int i = 0; i < SEND_SIZE; i++) {
		std::cout << std::setw(2) << std::hex << (int)data[i] << ":";
	}
}


int n_matches = 0;

int main(int argc, char* argv[]) {
	if (argc != 3) {
		std::cout << "[Error] Usage: " << argv[0]
			<< " [Comm Port] [baud]" << std::endl;
		return -1;
	}

	/*
	std::vector<unsigned char> testData({ 0x00, 0xBE, 0x04, 0x00, 0x00, 0x00, 0x01, 0x00, 0xFF, 0xFF });

	while (true) {
		for (auto& byte : testData) {
			auto packets = parsePacket({ byte });
			for (auto& packet : packets) {
				//std::cout << "[Info] Found packet with type " << (int)packet.type
					//<< " and length " << (int)packet.payload.size() << std::endl;
				std::cout << "Found packet:\n" << packet.toString() << "\n" << std::endl;
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}
	*/

	int port = std::stoi(std::string(argv[1])),
		baud = std::stoi(std::string(argv[2]));

	//Create SerialPort object
	SerialPort serial(port, baud);

	//Connect to serial device
	try {
		serial.Connect();
	}
	catch (const std::exception& e) {
		std::cout << "[Error] trying to connect: " << e.what() << std::endl;

		return -1;
	}

	while (serial.IsConnected()) {
		auto packets = parsePacket(serial.Read());

		for (auto& packet : packets) {
			std::cout << '.';
			//std::cout << "Found packet:\n" << packet.toString() << "\n" << std::endl;
		}
	}


	/*
	std::vector<unsigned char> send_data;
	for (int i = 0; i < SEND_SIZE; i++) {
		send_data.push_back(i % 256);
	}

	std::vector<unsigned char> recvd_data;

	std::chrono::milliseconds last_sent = std::chrono::milliseconds(0);
	while (serial.IsConnected()) {
		std::chrono::milliseconds now_time = std::chrono::duration_cast< std::chrono::milliseconds >(
					std::chrono::system_clock::now().time_since_epoch());
		if ((now_time - last_sent) > std::chrono::milliseconds(DELAY)) {
			serial.Send(send_data);
			last_sent = now_time;
		}

		auto read = serial.Read();
		recvd_data.insert(recvd_data.end(), read.begin(), read.end());

		if (recvd_data.size() >= SEND_SIZE) {
			bool matches = true;
			for (int i = 0; i < SEND_SIZE; i++) {
				if (recvd_data[i] != send_data[i]) {
					matches = false;
				}
			}
			if (!matches) {
				std::cout << "Mismatch on receive" << std::endl;
				std::cout << "Successfully matched " << n_matches << std::endl;

				std::cout << "Expected ";
				printHex(send_data);
				std::cout << std::endl;
				std::cout << "Received ";
				printHex(recvd_data);
				std::cout << std::endl;
			}
			else {
				std::cout << "Matches" << std::endl;
				n_matches++;
			}
			recvd_data = { recvd_data.begin() + SEND_SIZE, recvd_data.end() };
		}

		if (kbhit()) {
			exit(0);
			//unsigned char c = (unsigned char)getch();
			//serial.Send({ c });
			//std::cout << "Sent " << c << std::endl;
		}
	}

	*/
	return 0;
}

