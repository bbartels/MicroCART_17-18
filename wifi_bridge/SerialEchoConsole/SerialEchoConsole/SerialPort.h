#pragma once

#include <vector>
#include <string>
#include <Windows.h>
#include <exception>

class SerialPort {
public:
	static const int ERROR_SETTING_COMM_SETTINGS = -1;
	static const int ERROR_SETTING_COMM_TIMEOUTS = -2;
	static const int ERROR_OPENING_COMM_PORT = -3;
	static const int ERROR_SENDING_COMM = -4;
	static const int ERROR_PORT_NOT_READY = -5;
	static const int ERROR_GENERIC_WRITE_FAIL = -6;
	static const int ERROR_GENERIC_READ_FAIL = -7;
	static const int ERROR_GIVEN_NULL_POINTER = -8;

	class Exception : std::exception {
	public:
		Exception(std::string message) { _msg = std::string("SerialPort Exception: " + message); }

		virtual const char *what() { return _msg.c_str(); }
	private:
		std::string _msg;
	};

	SerialPort(unsigned char id, unsigned int baud);

	void Connect();
	void Disconnect();

	bool IsConnected();

	void Send(std::vector<unsigned char> data);
	std::vector<unsigned char> Read();

	~SerialPort();
private:
	HANDLE comHandle;
	unsigned int baud, port;
	bool connected;
};