#include "stdafx.h"
#include "Packet.hpp"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <chrono>

using namespace std;

Packet::Packet() {
	isValid = false;
}

Packet::Packet(uint8_t _type, uint8_t _subtype, uint16_t _id,
	const vector<unsigned char>& _payload)
	: type{ _type }
	, subtype{ _subtype }
	, id{ _id }
	, payload{ _payload }
	, isValid{ true } {

	checksum = 0xBE;
	
	checksum ^= type;
	checksum ^= subtype;
	checksum ^= (id >> 8);
	checksum ^= (id & 0xFF);
	checksum ^= (payload.size() >> 8) & 0xFF;
	checksum ^= (payload.size() & 0xFF);
	
	for (auto& byte : payload) {
		checksum ^= byte;
	}
}

Packet::operator bool() const {
	return isValid;
}


string Packet::toString() const {
	stringstream ss;
	ss << "[ID]\t" + to_string(id) +
		"\n[Type]\t" + to_string(type) +
		"\n[Subtype]\t" + to_string(subtype) +
		"\n[Length]\t" + to_string(payload.size()) +
		"\n[Payload]\t{ ";


	for (auto& byte : payload) {
		ss << std::hex << (int)byte << " ";
	}

	ss << "}";

	return ss.str();
}

vector<unsigned char> Packet::toVector() const {
	vector<unsigned char> buf({ 0xBE, type, subtype, static_cast<uint8_t>(id & 0xFF), static_cast<uint8_t>(id >> 8) });

	buf.insert(buf.end(), payload.begin(), payload.end());
	buf.push_back(checksum);

	return buf;
}

vector<Packet> parsePacket(const vector<unsigned char>& bytes) {
	static vector<unsigned char> savedBytes;
	static uint16_t id = 0;
	static bool first = true;
	static chrono::high_resolution_clock::time_point lastTime;

	if (bytes.size() == 0)
		return vector<Packet>();

	/*
	auto now = chrono::high_resolution_clock::now();

	if (first) {
		first = false;
	}
	else {
		auto durationMs = chrono::duration_cast<chrono::microseconds>(now - lastTime);
		double us = chrono::duration<double, micro>(durationMs).count();

		double baudrate = 10000000. * bytes.size() / us;

		if (baudrate > (640. / 0.005)) {
			cout << us << endl;
			cout << "[Warning] High baud rate: " << baudrate << endl;
		}
	}

	lastTime = now;
	*/

	if (bytes.size() > 64)
		cout << '|';
	
	vector<Packet> foundPackets;
	
	savedBytes.insert(savedBytes.end(), bytes.begin(), bytes.end());

	Packet p;
	while ((p = findPacket(savedBytes))) {
		foundPackets.push_back(p);

		if (p.id != id) {
			cout << "[Warning] Unexpected packet ID (received " << p.id << ", expected " << id << "):\n" << p.toString() << endl;
		}

		id = p.id + 1;
	}

	return foundPackets;
}

Packet findPacket(vector<unsigned char>& bytes) {
	static bool largePacket = false;
	Packet found;
	static chrono::high_resolution_clock::time_point lastTime = chrono::high_resolution_clock::now();
	static double timeFilter = 50.;

	if (bytes.size() == 0)
		return found;

	//Find start byte
	unsigned int i;
	for (i = 0; i < bytes.size() && bytes[i] != 0xBE; ++i);

	if (i == bytes.size()) {
		//No start byte
		cout << "Removing junk data (" << i << " bytes)" << endl;

		bytes.clear();
		return found;
	}

	//Remove junk data
	if (i != 0) {
		std::cout << "Removing junk data (" << i << " bytes)" << std::endl;
		bytes.erase(bytes.begin(), bytes.begin() + i);
	}

	if (bytes.size() >= 7) {
		uint16_t length = bytes[5] | (bytes[6] << 8);

		if (length > 100 && !largePacket) {
			largePacket = true;

			cout << "[Warning] Receiving large packet (size " << length << ")" << endl;
		}

		if (bytes.size() >= (8 + length)) {
			//We have a full packet!
			vector<unsigned char> payload(bytes.begin() + 7, bytes.begin() + 7 + length);

			found = Packet(bytes[1], bytes[2], bytes[3] | (bytes[4] << 8), payload);

			if (found.checksum != bytes[7 + length]) {
				cout << "[Error] Invalid checksum:\n" << found.toString();
				found.isValid = false;
			}

			//Remove the packet data
			bytes.erase(bytes.begin(), bytes.begin() + 8 + length);

			//Clear the large packet flag
			largePacket = false;

			auto now = chrono::high_resolution_clock::now();

			auto ms = chrono::duration_cast<chrono::milliseconds>(now - lastTime);
			double duration = chrono::duration<double, milli>(ms).count();

			double baud = 10000. * (8 + length) / duration;

			if (duration > (2 * timeFilter)) {
				cout << "[Warning] Jitter: Average duration " << (int)timeFilter << "ms, this one " << (int)duration << "ms" << endl;
			}

			timeFilter = timeFilter*0.5 + duration*0.5;

			lastTime = now;
		}
	}

	return found;
}