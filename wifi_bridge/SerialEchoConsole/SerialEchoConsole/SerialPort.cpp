#include "stdafx.h"
#include "SerialPort.h"

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

SerialPort::SerialPort(unsigned char id, unsigned int baud) {
	comHandle = INVALID_HANDLE_VALUE;
	connected = false;

	this->port = id;
	this->baud = baud;

	//Connect();
}

void SerialPort::Connect() {
	DCB dcb;
	COMMTIMEOUTS timeouts;

	std::string portName = "\\\\.\\COM" + std::to_string(port);

	if (connected || (comHandle != INVALID_HANDLE_VALUE))
		throw Exception("SerialPort already connected.");

	comHandle = CreateFile(portName.c_str(), GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);

	if (comHandle == INVALID_HANDLE_VALUE)
		throw Exception("Unable to open COM port.");

	GetCommState(comHandle, &dcb);

	dcb.BaudRate = baud;
	dcb.fBinary = 1;
	dcb.Parity = NOPARITY;
	dcb.StopBits = TWOSTOPBITS;
	dcb.ByteSize = 8;

	if (!SetCommState(comHandle, &dcb)) {
		Disconnect();
		
		throw Exception("Unable to set COM port parameters.");
	}

	timeouts.ReadIntervalTimeout = MAXDWORD;
	timeouts.ReadTotalTimeoutMultiplier = MAXDWORD;
	timeouts.ReadTotalTimeoutConstant = 1;
	timeouts.WriteTotalTimeoutConstant = 1;
	timeouts.WriteTotalTimeoutMultiplier = 1;

	if (!SetCommTimeouts(comHandle, &timeouts)) {
		Disconnect();
		
		throw Exception("Unable to set COM port timeouts.");
	}

	connected = true;
}

void SerialPort::Disconnect() {
	if (!connected)
		return;

	CloseHandle(comHandle);
	
	comHandle = INVALID_HANDLE_VALUE;
	connected = false;
}

bool SerialPort::IsConnected() {
	return connected;
}

void SerialPort::Send(std::vector<unsigned char> data) {
	DWORD nWritten;
	unsigned int totalSent = 0, attempts = 0;

	if (!IsConnected())
		throw Exception("SerialPort not connected.");

	while (totalSent < data.size()) {
		bool failed = false;
		if (!WriteFile(comHandle, (LPCVOID)(data.data() + totalSent), data.size() - totalSent, &nWritten, NULL))
			failed = true;

		if (attempts++ > 10)
			failed = true;

		if (failed) {
			Disconnect();
			throw Exception("Unable to send data.");
		}
		totalSent += nWritten;
	}
}

std::vector<unsigned char> SerialPort::Read() {
	if (!IsConnected())
		throw Exception("SerialPort not connected.");

	DWORD nRead;
	unsigned char buffer[32];

	if (!ReadFile(comHandle, (LPVOID)buffer, 32, &nRead, NULL)) {
		Disconnect();
		throw Exception("Unable to receive data.");
	}

	std::vector<unsigned char> data;
	data.assign(buffer, buffer + nRead);

	return data;
}

SerialPort::~SerialPort() {
	if (IsConnected())
		Disconnect();
}