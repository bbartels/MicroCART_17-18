Title: First pass of CLI
Date: 2016-10-28
Category: Highlights
Authors: Brendan
thumbnail: "/images/cli.png"

One of our objectives on the Ground Station was to develop a command line interface to interact with the ground station back end. Kris and Jake have given us a sneak peek into the CLI they are developing.

<a href="/images/cli.png">
<figure>
<img src="images/cli.png">
</figure>
</a>