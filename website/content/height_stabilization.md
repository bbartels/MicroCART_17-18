Title: Height Stabilization (movie)
Date: 2016-12-04
Authors: Brendan
Category: Highlights
image:

The following test flight demonstrates altitude stabilization. Once the pilot puts the quadcopter into autonomous mode, the quadcopter uses its internal control algorithm to maintain its altitude. The pilot still must provide x and y-axis stabilization through the remote control.

<figure>
<video controls>
<source src="/videos/height_stabilization.mp4">
</video>
</figure>
