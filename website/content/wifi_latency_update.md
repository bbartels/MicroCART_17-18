Title: Wi-Fi Latency Testing
Date: 2016-09-22
Authors: Brendan
Category: Highlights
thumbnail: "/images/wifi_testing.png"

Historically, all wireless communication to the quadcopter has been accomplished through bluetooth. We've noticed that the bluetooth one-way latency was around 60-80 ms on average, which is not quite fast enough for our needs, especially if any control algorithm computations are to be performed on the ground station.

David took up the challenge to test a promising solution using Wi-Fi instead of Bluetooth, and his initial tests revealed some promising metrics.

<a href="/images/wifi_testing.png">
<figure>
<img src="/images/wifi_testing.png">
</figure>
</a>

He tested communication with both UDP and TCP protocols, but more importantly the one-way latency for both protocols was far less than our bluetooth setup, with most of the WiFi latency distrubtion falling below 20ms.
