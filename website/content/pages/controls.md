Title: Controls
sortorder: 015

# Controls

The quadcopter is stabilized with on-board PID controllers. These controllers
use pitch, roll, and yaw data from on-board sensors and positional data from the
infrared tracking system to determine appropriate actuator signals necessary
to keep the quadcopter stable during flight.

The classical PID controller is characterized by 3 coefficients that specify
how the controller functions. A PID controller can be configured through
iterative determination of the PID coefficients, but we expect to obtain a more
accurate control system by first developing a robust mathematical model of the
quadcopter system. Once we establish this model, we can use numerical
characteristics of the quadcopter to calculate the appropriate PID coefficients
for our controllers.

The model we are developing follows the methods developed by our advising
graduate student Matt Rich in his thesis [Model development, system
identification, and control of a quadrotor helicopter][1].

<img src="/images/pid_diagram.png" width="700">

[1]: http://lib.dr.iastate.edu/etd/12770/
