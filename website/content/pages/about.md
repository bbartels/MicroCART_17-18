Title: About
url:
save_as: index.html
date: 2016-11-19
sortorder: 001

# MicroCART
## Microprocessor Controlled Aerial Robot Team

**Senior Design Group**: may1716

<img align="right" src="/images/quad_pic.png" width="40%">

MicroCART is an ongoing senior design project focused on the
development of a quadcopter as a research platform for controls and
embedded systems. This year, our team is responsible for advancing the
modular structure the platform, developing a controls model, and
improving the autonomous flight capabilities of the quadcopter.

For additional information regarding the project, please visit the
[supplemental wiki
page][1].

[1]: https://wikis.ece.iastate.edu/microcart-senior-design/index.php/2016-2017_Main_Page
