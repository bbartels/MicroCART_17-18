Title: CLI Improved
Date: 2016-11-29
Authors: Brendan
Category: Highlights
thumbnail: "/images/new_cli.png"

Jake and Kris have continued their work on the command line interface and have provided another sample of the most recent CLI functionality.

<a href="/images/new_cli.png">
<figure>
<img src="/images/new_cli.png">
</figure>
</a>

This example shows some of the commands used to control the PID constants of the quadcopter. On the right top are the commands being executed; on the left is the backend daemon; and on the right bottom is a continuously running monitor that refreshes 10 times per second.
