#!/bin/bash

HOST=may1716.sd.ece.iastate.edu
USERNAME=may1716

if [ ! -e PASSWORD ]; then
    echo "

*****
ERROR: File PASSWORD is missing.
Create a file called PASSWORD in the website root directory, and put the sftp
password in that file. No spaces. No newlines.
(The SFTP password was given to us in an email. Just search 'sftp password' in
your CyMail and it will probably be first result.)
*****

"
    exit 1
fi

PASSWORD=$(cat PASSWORD)

chmod -R 755 output || exit 1
#cd output || exit 1

echo "----"
echo "sftp password (copy and paste this into the password prompt)"
echo $PASSWORD
echo "----"

sftp -o PubkeyAuthentication=false $USERNAME@$HOST <<EOF
put -r output/* www/
exit
EOF
