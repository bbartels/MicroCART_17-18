#!/bin/bash

[ -d bin/ ] && rm -rf bin/ lib/ include/

/remote/python/2.7.9/bin/virtualenv ./
source bin/activate
pip install --upgrade pip
pip install pelican
pip install Markdown
