#ifndef __FRONTEND_GETSETPOINT_H
#define __FRONTEND_GETSETPOINT_H

#include "frontend_common.h"
#include "setpoint_common.h"

/* Get a specified setpoint.
 *
 * Example:
 *
 * struct frontend_setpoint_data setpoint_data;
 * if (frontend_getpid(conn, &pid_data)) {
 * 		error
 * } else {
 * 		setpoint_data.height, setpoint_data.lat, and setpoint.long are filled
 * }
 *
 * Returns 0 on success, 1 on error
 */
int frontend_getsetpoint(
		struct backend_conn * conn,
		struct frontend_setpoint_data * setpoint_data,
		int type);

#endif /* __FRONTEND_GETPID_H */