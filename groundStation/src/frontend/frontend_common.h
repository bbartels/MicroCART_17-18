#ifndef __FRONTEND_COMMON_H
#define __FRONTEND_COMMON_H
#include <stdlib.h>

struct backend_conn;

/* Start connection to quad */
struct backend_conn * ucart_backendConnect(void);

/* Stop connection to quad */
void ucart_backendDisconnect(struct backend_conn * conn);

/* Get a line from the backend.
 *
 * The line will remain valid until the next call to ucart_backendGetline.
 */
char * ucart_backendGetline(struct backend_conn * conn);

/* Write a line to the backend */
int ucart_backendWrite(struct backend_conn * backend, const char * line);

#endif /* __FRONTEND_COMMON_H */