#ifndef __FRONTEND_GETPID_H
#define __FRONTEND_GETPID_H

#include "frontend_common.h"
#include "pid_common.h"

/* Get a specified PID.
 *
 * Example:
 *
 * struct frontend_pid_data pid_data;
 * pid_data.pid = PITCH;
 * if (frontend_getpid(conn, &pid_data)) {
 * 		error
 * } else {
 * 		pid_data.p, pid_data.i, and pid_data.d are filled
 * }
 *
 * Returns 0 on success, 1 on error
 */
int frontend_getpid(
		struct backend_conn * conn,
		struct frontend_pid_data * pid_data);


#endif /* __FRONTEND_GETPID_H */