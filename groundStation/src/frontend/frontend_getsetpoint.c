#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "frontend_getsetpoint.h"
#include "setpoint_common.h"
#include "cli_getsetpoint.h"

/* Get a specified setpoint.
 *
 * Example:
 *
 * struct frontend_setpoint_data setpoint_data;
 * if (frontend_getpid(conn, &pid_data)) {
 * 		error
 * } else {
 * 		setpoint_data.height, setpoint_data.lat, and setpoint.long are filled
 * }
 *
 * Returns 0 on success, 1 on error
 */
int frontend_getsetpoint(
		struct backend_conn * conn, struct frontend_setpoint_data * setpoint_data, int type) {
	
	char line[25] = "";
	switch (type) {
		case HEIGHT :
			strncpy(line, "getheight\n", 10);
			break;
		case LAT :
			strncpy(line, "getlat\n", 7);
			break;
		case LONGG :
			strncpy(line, "getlong\n", 8);
			break;
		case PITCH :
			strncpy(line, "getpitch\n", 9);
			break;
		case ROLL :
			strncpy(line, "getroll\n", 8);
			break;
		case YAW :
			strncpy(line, "getyaw\n", 7);
			break;
	}
	int size;
	if((size = ucart_backendWrite(conn, line)) < 0 ) {
		return 1;
	}

	return 0;
}	
