#ifndef __FRONTEND_SETSETPOINT_H
#define __FRONTEND_SETSETPOINT_H

#include "setpoint_common.h"
#include "frontend_common.h"

int frontend_setsetpoint(
		struct backend_conn * conn,
		struct frontend_setpoint_data * setpoint_data,
		int mask);

#define SET_LAT 0x01
#define SET_LONG 0x02
#define SET_HEIGHT 0x04
#define SET_PITCH 0x08
#define SET_ROLL  0x10
#define SET_YAW   0x20

#ifndef SET_ALL
#define SET_ALL (SET_LAT | SET_LONG | SET_HEIGHT | SET_PITCH | SET_ROLL | SET_YAW)
#endif /* SET_ALL */

#endif /* __FRONTEND_SETSETPOINT_H */