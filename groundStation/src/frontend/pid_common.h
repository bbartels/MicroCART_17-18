#ifndef __PID_COMMON_H
#define __PID_COMMON_H

enum pid_controller {
	PID_PITCH,
	PID_ROLL,
	PID_YAW,
	PID_PITCH_RATE,
	PID_ROLL_RATE,
	PID_YAW_RATE,
	PID_HEIGHT, /* Z */
	PID_LAT, /* X */
	PID_LONG, /* Y */
	PID_NUM_PIDS
};

struct frontend_pid_data {
	enum pid_controller controller;

	float p;
	float i;
	float d;
};


#endif /* __PID_COMMON_H */
