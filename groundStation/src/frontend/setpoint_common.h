#ifndef __SETPOINT_COMMON_H
#define __SETPOINT_COMMON_H

struct frontend_setpoint_data {
	float height;
	float lat;
	/* named with two g's due to long type name */
	float longg;
	float pitch;
	float roll;
	float yaw;
};


#endif /* __SETPOINT_COMMON_H */