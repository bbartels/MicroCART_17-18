#ifndef __FRONTEND_SETPID_H
#define __FRONTEND_SETPID_H

#include "pid_common.h"
#include "frontend_common.h"

int frontend_setpid(
		struct backend_conn * conn,
		struct frontend_pid_data * pid_data,
		int mask);

#define SET_P 0x01
#define SET_I 0x02
#define SET_D 0x04

#ifndef SET_ALL
#define SET_ALL (SET_P | SET_I | SET_D)
#endif /* SET_ALL */

#endif /* __FRONTEND_SETPID_H */