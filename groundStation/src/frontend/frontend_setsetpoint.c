#include <err.h>
#include <stdio.h>

#include "frontend_setsetpoint.h"
#include "setpoint_common.h"
#include "frontend_common.h"

int frontend_setsetpoint(
		struct backend_conn * conn,
		struct frontend_setpoint_data * setpoint_data,
		int mask)
{
	if (conn == NULL) {
		return -1;
	}

	char buffer[2048];
	/* Set the P, I, and D */
	if (mask & SET_HEIGHT) {
		if (snprintf(buffer, 2048,
					"setheight %f\n",  
					setpoint_data->height) >= 2048) {
			errx(0, "Buffer to small to format!");
		}
		ucart_backendWrite(conn, buffer);
	}
	if (mask & SET_LAT) {
		if (snprintf(buffer, 2048,
					"setlat %f\n",
					setpoint_data->lat) >= 2048) {
			errx(0, "Buffer to small to format!");
		}
		ucart_backendWrite(conn, buffer);
	}
	if (mask & SET_LONG) {
		if (snprintf(buffer, 2048,
					"setlong %f\n",
					setpoint_data->longg) >= 2048) {
			errx(0, "Buffer to small to format!");
		}
		ucart_backendWrite(conn, buffer);
	}
	if (mask & SET_PITCH) {
		if (snprintf(buffer, 2048,
					"setpitch %f\n",  
					setpoint_data->pitch) >= 2048) {
			errx(0, "Buffer to small to format!");
		}
		ucart_backendWrite(conn, buffer);
	}
	if (mask & SET_ROLL) {
		if (snprintf(buffer, 2048,
					"setroll %f\n",
					setpoint_data->roll) >= 2048) {
			errx(0, "Buffer to small to format!");
		}
		ucart_backendWrite(conn, buffer);
	}
	if (mask & SET_YAW) {
		if (snprintf(buffer, 2048,
					"setyaw %f\n",
					setpoint_data->yaw) >= 2048) {
			errx(0, "Buffer to small to format!");
		}
		ucart_backendWrite(conn, buffer);
	}

	return 0;
}
