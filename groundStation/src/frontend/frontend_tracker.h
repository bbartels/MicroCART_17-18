#ifndef __FRONTEND_TRACKER_H
#define __FRONTEND_TRACKER_H

#include "frontend_common.h"

/* Struct containing pos/att data */
struct frontend_tracker_data {
		double height;
		double lateral;
		double longitudinal;
		double pitch;
		double roll;
		double yaw;
};

/* Get pos/att data from the tracking system
 *
 * conn: IN Connection to quad
 * data: OUT Data is written to this struct
 *
 * Returns 0 on success, nonzero on error
 *
 */
int frontend_track(struct backend_conn * conn, 
		struct frontend_tracker_data *data);

#endif /* __FRONTEND_TRACKER_H */