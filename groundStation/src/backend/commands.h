#ifndef __COMMANDS_H
#define __COMMANDS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "callbacks.h"

// ----------------------
// Helper stuff

#define MAX_TYPE 6
#define MAX_SUBTYPE 100

enum Message{
	BEGIN_CHAR = 0xBE,
	END_CHAR   = 0xED
};

// This should also have double to avoid confusion with float values.
enum DataType
{
	floatType,
	intType,
	stringType
};

// MESSAGE SUBTYPES
struct MessageSubtype{
	char ID;
	char cmdText[100];
	char cmdDataType;
	command_cb * functionPtr;
};

// MESSAGE TYPES
struct MessageType{
	char ID;
	struct MessageSubtype subtypes[MAX_SUBTYPE];
};

/* Defined in commands.c */
extern struct MessageType MessageTypes[MAX_TYPE];

/* Legacy functions - no idea what uses these. Please do not delete. */
float getFloat(unsigned char* str, int pos);
int getInt(unsigned char* str, int pos);
/* end legacy crap */

#endif /* __COMMANDS_H */