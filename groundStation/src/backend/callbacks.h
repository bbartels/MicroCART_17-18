#ifndef __CALLBACKS_H
#define __CALLBACKS_H

/* Grab some stupid stuff from legacy code */
#include "type_def.h"

/* Make commands.c happy */
typedef void (command_cb)(unsigned char *command, 
		int dataLen, modular_structs_t *structs);

#endif /* __CALLBACKS_H */
