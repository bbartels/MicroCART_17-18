/* Author: Kris Burney
 *
 * Logger file for holding functions pertaining to loging to a file.
 */
#include "logger.h"
#include <stdio.h>
#include <err.h>
#include <pthread.h>

static FILE * quadlog_file = NULL;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int createLogFile(int argc, char* argv)
{
	if (quadlog_file != NULL) {
		return -1;
	}

	if (pthread_mutex_lock(&mutex)) {
		err(-2, "pthrtead_mutex_lock (%s:%d):", __FILE__, __LINE__);
	}

	char log_file[300];
	strcpy(log_file, "logs/");
	if(argc >= 2)
	{
		strncat(log_file, argv, 294);
		printf("Creating log file '%s'...\n",log_file);
		quadlog_file = fopen(log_file, "a");
	} else {
		time_t rawtime;
		char timestr [30];
		time(&rawtime);
		sprintf(timestr,"%s",ctime(&rawtime));

		// Lets convert space to _ in
		char *p = timestr;
		size_t i = 0;
		while(i < strlen(timestr))
		{ 
		    if (*p == ' ')
		          *p = '_';
			i++;
			p++;
		}

		// timestr has a weird char at the end of it.
		// we will not include it in our file name
		strncat(log_file, timestr, strlen(timestr) -1 );
		strcat(log_file, ".txt");
		printf("Creating log file '%s'...\n",log_file);
		quadlog_file = fopen(log_file, "a");
	}
	if (pthread_mutex_unlock(&mutex)) {
		err(-2, "pthrtead_mutex_unlock (%s:%d):", __FILE__, __LINE__);
	}
	return 0;
}

int updateLogFile(const struct ucart_vrpn_TrackerData * td)
{
	int retval;

	if (pthread_mutex_lock(&mutex)) {
		err(-2, "pthrtead_mutex_lock (%s:%d):", __FILE__, __LINE__);
	}
     
	retval = fprintf(quadlog_file, 
			"FPS: %lf Pos (xyz): (%lf %lf %lf) Att (pry): (%lf %lf %lf)\n",
		td->fps, td->x, td->y, td->z, td->pitch, td->roll, td->yaw);

	if (pthread_mutex_unlock(&mutex)) {
		err(-2, "pthrtead_mutex_unlock (%s:%d):", __FILE__, __LINE__);
	}
	
	return retval;
}

int writeStringToLog(const char * string)
{
	int retval;

	if (pthread_mutex_lock(&mutex)) {
		err(-2, "pthrtead_mutex_lock (%s:%d):", __FILE__, __LINE__);
	}

	retval = fprintf(quadlog_file, "%s", string);

	if (pthread_mutex_unlock(&mutex)) {
		err(-2, "pthrtead_mutex_unlock (%s:%d):", __FILE__, __LINE__);
	}

	return retval;
}

void closeLogFile(void)
{
	if (pthread_mutex_lock(&mutex)) {
		err(-2, "pthrtead_mutex_lock (%s:%d):", __FILE__, __LINE__);
	}

	fclose(quadlog_file);

	if (pthread_mutex_unlock(&mutex)) {
		err(-2, "pthrtead_mutex_unlock (%s:%d):", __FILE__, __LINE__);
	}

}
