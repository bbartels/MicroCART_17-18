#ifndef __COMMUNICATION_H
#define __COMMUNICATION_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include "type_def.h"

tokenList_t tokenize(char* cmd);
int checkFloat(char *floatString, float *value);
int checkInt(char *intString, int *value);
int formatCommand(char *command, unsigned char **formattedCommand);
int formatPacket(metadata_t *metadata, void *data, unsigned char **formattedCommand);
int parse_packet(unsigned char * packet, unsigned char ** data, metadata_t * meta_data);
int processCommand(unsigned char *command, modular_structs_t *structs);
int logData(unsigned char *log_msg, unsigned char *formattedCommand);

#endif /* __COMMUNICATION_H */