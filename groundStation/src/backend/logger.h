/* Author: Kris Burney
 *
 * Logger header file for holding info pertaining to loging to a file.
 */
#ifndef __LOGGER_H
#define __LOGGER_H

#include <fcntl.h>
#include <time.h>
#include <stdio.h>
#include <string.h>

#include "vrpn_tracker.hpp"

int createLogFile(int, char*);
int writeStringToLog(const char*);
int updateLogFile(const struct ucart_vrpn_TrackerData* );
void closeLogFile();


#endif /* __LOGGER_H */