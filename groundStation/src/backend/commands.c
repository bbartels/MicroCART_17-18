#include "commands.h"

/* This file defines the commands structure.
 * This is the canonical reference for all commands
 * used. This file can be used - unchanged - on both the quad side
 * and on the ground station side. The commands.h file is also entirely
 * portable and may be used unchanged.
 *
 * This file (commands.c) and the matching header (commands.h)
 * are fully portable (quad + groundStation).
 * 
 * To use this file, three non-portable files must also exist:
 *  - callbacks.h. Typedef for command_cb
 *  - cb_default.h. Implementation of cb_default.
 *  - callbacks.c file. Contains implemented callbacks.
 *
 * There are two mandatory things that must be implemented in
 * other files for this to work: First, in callbacks.h, create a typedef
 * for command_cb. See the callbacks.h of the ground station for an
 * example.
 *
 * Second, in cb_default.h, implement the function
 * cb_default. This function should do nothing; it will be the 
 * default action for an unimplemented callback. Note that because
 * the function is implemented in the .h file, cb_default.h MUST NOT
 * be included in any other file!
 *
 * To implement callbacks, simply define them in callbacks.c.
 *
 *
 * EXTENDING COMMANDS.C
 *
 * To extend this file, simply add the new subtypes (typically
 * a Setter, Getter, and Response) and create weak aliases below.
 *
 * Ensure the Quad and GroundStation always maintain this file in sync!
 *
 */

/* List of callbacks. DO NOT MODIFY THESE IN THIS FILE - 
 * Simply implement a function with the same name
 * in a different file (callbacks.c) and these will
 * be overridden.
 */

/* Grab the default callback implementation */
#include "cb_default.h"

/* Misc. callbacks */
command_cb cb_debug __attribute__((weak, alias("cb_default")));
command_cb cb_packetlog __attribute__((weak, alias("cb_default")));
command_cb cb_getpacketlogs __attribute__((weak, alias("cb_default")));
command_cb cb_update __attribute__((weak, alias("cb_default")));
command_cb cb_beginupdate __attribute__((weak, alias("cb_default")));
command_cb cb_log __attribute__((weak, alias("cb_default")));
command_cb cb_response __attribute__((weak, alias("cb_default")));

/* Callbacks for configuration (setters)*/
command_cb cb_setyawp __attribute__((weak, alias("cb_default")));
command_cb cb_setyawi __attribute__((weak, alias("cb_default")));
command_cb cb_setyawd __attribute__((weak, alias("cb_default")));
command_cb cb_setrollp __attribute__((weak, alias("cb_default")));
command_cb cb_setrolli __attribute__((weak, alias("cb_default")));
command_cb cb_setrolld __attribute__((weak, alias("cb_default")));
command_cb cb_setpitchp __attribute__((weak, alias("cb_default")));
command_cb cb_setpitchi __attribute__((weak, alias("cb_default")));
command_cb cb_setpitchd __attribute__((weak, alias("cb_default")));
command_cb cb_setyawratep __attribute__((weak, alias("cb_default")));
command_cb cb_setyawratei __attribute__((weak, alias("cb_default")));
command_cb cb_setyawrated __attribute__((weak, alias("cb_default")));
command_cb cb_setrollratep __attribute__((weak, alias("cb_default")));
command_cb cb_setrollratei __attribute__((weak, alias("cb_default")));
command_cb cb_setrollrated __attribute__((weak, alias("cb_default")));
command_cb cb_setpitchratep __attribute__((weak, alias("cb_default")));
command_cb cb_setpitchratei __attribute__((weak, alias("cb_default")));
command_cb cb_setpitchrated __attribute__((weak, alias("cb_default")));
command_cb cb_setlatp __attribute__((weak, alias("cb_default")));
command_cb cb_setlati __attribute__((weak, alias("cb_default")));
command_cb cb_setlatd __attribute__((weak, alias("cb_default")));
command_cb cb_setlongp __attribute__((weak, alias("cb_default")));
command_cb cb_setlongi __attribute__((weak, alias("cb_default")));
command_cb cb_setlongd __attribute__((weak, alias("cb_default")));
command_cb cb_setheightp __attribute__((weak, alias("cb_default")));
command_cb cb_setheighti __attribute__((weak, alias("cb_default")));
command_cb cb_setheightd __attribute__((weak, alias("cb_default")));
command_cb cb_setheight __attribute__((weak, alias("cb_default")));
command_cb cb_setlat __attribute__((weak, alias("cb_default")));
command_cb cb_setlong __attribute__((weak, alias("cb_default")));
command_cb cb_setyaw __attribute__((weak, alias("cb_default")));
command_cb cb_setpitch __attribute__((weak, alias("cb_default")));
command_cb cb_setroll __attribute__((weak, alias("cb_default")));

/* Default callbacks for getters */
command_cb cb_getyawp __attribute__((weak, alias("cb_default")));
command_cb cb_getyawi __attribute__((weak, alias("cb_default")));
command_cb cb_getyawd __attribute__((weak, alias("cb_default")));
command_cb cb_getrollp __attribute__((weak, alias("cb_default")));
command_cb cb_getrolli __attribute__((weak, alias("cb_default")));
command_cb cb_getrolld __attribute__((weak, alias("cb_default")));
command_cb cb_getpitchp __attribute__((weak, alias("cb_default")));
command_cb cb_getpitchi __attribute__((weak, alias("cb_default")));
command_cb cb_getpitchd __attribute__((weak, alias("cb_default")));
command_cb cb_getyawratep __attribute__((weak, alias("cb_default")));
command_cb cb_getyawratei __attribute__((weak, alias("cb_default")));
command_cb cb_getyawrated __attribute__((weak, alias("cb_default")));
command_cb cb_getrollratep __attribute__((weak, alias("cb_default")));
command_cb cb_getrollratei __attribute__((weak, alias("cb_default")));
command_cb cb_getrollrated __attribute__((weak, alias("cb_default")));
command_cb cb_getpitchratep __attribute__((weak, alias("cb_default")));
command_cb cb_getpitchratei __attribute__((weak, alias("cb_default")));
command_cb cb_getpitchrated __attribute__((weak, alias("cb_default")));
command_cb cb_getlatp __attribute__((weak, alias("cb_default")));
command_cb cb_getlati __attribute__((weak, alias("cb_default")));
command_cb cb_getlatd __attribute__((weak, alias("cb_default")));
command_cb cb_getlongp __attribute__((weak, alias("cb_default")));
command_cb cb_getlongi __attribute__((weak, alias("cb_default")));
command_cb cb_getlongd __attribute__((weak, alias("cb_default")));
command_cb cb_getheightp __attribute__((weak, alias("cb_default")));
command_cb cb_getheighti __attribute__((weak, alias("cb_default")));
command_cb cb_getheightd __attribute__((weak, alias("cb_default")));
command_cb cb_getheight __attribute__((weak, alias("cb_default")));
command_cb cb_getlat __attribute__((weak, alias("cb_default")));
command_cb cb_getlong __attribute__((weak, alias("cb_default")));
command_cb cb_getyaw __attribute__((weak, alias("cb_default")));
command_cb cb_getpitch __attribute__((weak, alias("cb_default")));
command_cb cb_getroll __attribute__((weak, alias("cb_default")));

/* Default callbacks for responses */
command_cb cb_respyawp __attribute__((weak, alias("cb_default")));
command_cb cb_respyawi __attribute__((weak, alias("cb_default")));
command_cb cb_respyawd __attribute__((weak, alias("cb_default")));
command_cb cb_resprollp __attribute__((weak, alias("cb_default")));
command_cb cb_resprolli __attribute__((weak, alias("cb_default")));
command_cb cb_resprolld __attribute__((weak, alias("cb_default")));
command_cb cb_resppitchp __attribute__((weak, alias("cb_default")));
command_cb cb_resppitchi __attribute__((weak, alias("cb_default")));
command_cb cb_resppitchd __attribute__((weak, alias("cb_default")));
command_cb cb_respyawratep __attribute__((weak, alias("cb_default")));
command_cb cb_respyawratei __attribute__((weak, alias("cb_default")));
command_cb cb_respyawrated __attribute__((weak, alias("cb_default")));
command_cb cb_resprollratep __attribute__((weak, alias("cb_default")));
command_cb cb_resprollratei __attribute__((weak, alias("cb_default")));
command_cb cb_resprollrated __attribute__((weak, alias("cb_default")));
command_cb cb_resppitchratep __attribute__((weak, alias("cb_default")));
command_cb cb_resppitchratei __attribute__((weak, alias("cb_default")));
command_cb cb_resppitchrated __attribute__((weak, alias("cb_default")));
command_cb cb_resplatp __attribute__((weak, alias("cb_default")));
command_cb cb_resplati __attribute__((weak, alias("cb_default")));
command_cb cb_resplatd __attribute__((weak, alias("cb_default")));
command_cb cb_resplongp __attribute__((weak, alias("cb_default")));
command_cb cb_resplongi __attribute__((weak, alias("cb_default")));
command_cb cb_resplongd __attribute__((weak, alias("cb_default")));
command_cb cb_respheightp __attribute__((weak, alias("cb_default")));
command_cb cb_respheighti __attribute__((weak, alias("cb_default")));
command_cb cb_respheightd __attribute__((weak, alias("cb_default")));
command_cb cb_respheight __attribute__((weak, alias("cb_default")));
command_cb cb_resplat __attribute__((weak, alias("cb_default")));
command_cb cb_resplong __attribute__((weak, alias("cb_default")));
command_cb cb_respyaw __attribute__((weak, alias("cb_default")));
command_cb cb_resppitch __attribute__((weak, alias("cb_default")));
command_cb cb_resproll __attribute__((weak, alias("cb_default")));

/* Command structure */
struct MessageType MessageTypes[MAX_TYPE] =
{
	// DEBUG
	{
		// Message Type ID
		0x00,

		// Debug Subtypes
		{
			// NONE subtype
			{
				// ID
				0x00,
				// Command text
				"debug",
				// Type of the command data
				stringType,
				// Function pointer
				&cb_debug
			},
			// NONE subtype
			{
				// ID
				0x01,
				// Command text
				"packetlog",
				// Type of the command data
				stringType,
				// Function pointer
				&cb_packetlog
			},
			// NONE subtype
			{
				// ID
				0x02,
				// Command text
				"getpacketlogs",
				// Type of the command data
				stringType,
				// Function pointer
				&cb_getpacketlogs
			}
		}
	},

	// CALIBRATION
	{
		// Message Type ID
		0x01,

		// Calibration Subtypes (PID coefficients)
		{
			// yaw p constant subtype
			{
				// ID
				0x00,
				// Command text
				"setyawp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setyawp
			},
			// yaw i constant subtype
			{
				// ID
				0x01,
				// Command text
				"setyawi",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setyawi
			},
			// yaw d constant subtype
			{
				// ID
				0x02,
				// Command text
				"setyawd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setyawd
			},
			// roll p constant subtype
			{
				// ID
				0x03,
				// Command text
				"setrollp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setrollp
			},
			// roll i constant subtype
			{
				// ID
				0x04,
				// Command text
				"setrolli",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setrolli
			},
			// roll d constant subtype
			{
				// ID
				0x05,
				// Command text
				"setrolld",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setrolld
			},
			// pitch p constant subtype
			{
				// ID
				0x06,
				// Command text
				"setpitchp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setpitchp
			},
			// pitch i constant subtype
			{
				// ID
				0x07,
				// Command text
				"setpitchi",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setpitchi
			},
			// pitch d constant subtype
			{
				// ID
				0x08,
				// Command text
				"setpitchd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setpitchd
			},
			// yawrate p constant subtype
			{
				// ID
				0x09,
				// Command text
				"setyawratep",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setyawratep
			},
			// yawrate i constant subtype
			{
				// ID
				0x0A,
				// Command text
				"setyawratei",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setyawratei
			},
			// yawrate d constant subtype
			{
				// ID
				0x0B,
				// Command text
				"setyawrated",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setyawrated
			},
			// rollrate p constant subtype
			{
				// ID
				0x0C,
				// Command text
				"setrollratep",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setrollratep
			},
			// rollrate i constant subtype
			{
				// ID
				0x0D,
				// Command text
				"setrollratei",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setrollratei
			},
			// rollrate d constant subtype
			{
				// ID
				0x0E,
				// Command text
				"setrollrated",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setrollrated
			},
			// pitchrate p constant subtype
			{
				// ID
				0x0F,
				// Command text
				"setpitchratep",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setpitchratep
			},
			// pitchrate i constant subtype
			{
				// ID
				0x10,
				// Command text
				"setpitchratei",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setpitchratei
			},
			// pitchrate d constant subtype
			{
				// ID
				0x11,
				// Command text
				"setpitchrated",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setpitchrated
			},
			// height p constant subtype
			{
				// ID
				0x12,
				// Command text
				"setheightp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setheightp
			},
			// height i constant subtype
			{
				// ID
				0x13,
				// Command text
				"setheighti",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setheighti
			},
			// height d constant subtype
			{
				// ID
				0x14,
				// Command text
				"setheightd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setheightd
			},
			// lat p constant subtype
			{
				// ID
				0x15,
				// Command text
				"setlatp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setlatp
			},
			// lat i constant subtype
			{
				// ID
				0x16,
				// Command text
				"setlati",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setlati
			},
			// lat d constant subtype
			{
				// ID
				0x17,
				// Command text
				"setlatd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setlatd
			},
			// long p constant subtype
			{
				// ID
				0x18,
				// Command text
				"setlongp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setlongp
			},
			// long i constant subtype
			{
				// ID
				0x19,
				// Command text
				"setlongi",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setlongi
			},
			// long d constant subtype
			{
				// ID
				0x1A,
				// Command text
				"setlongd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setlongd
			},
			// height setpoint subtype
			{
				// ID
				0x1B,
				// Command text
				"setheight",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setheight
			},
			// lat setpoint subtype
			{
				// ID
				0x1C,
				// Command text
				"setlat",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setlat
			},
			// long setpoint subtype
			{
				// ID
				0x1D,
				// Command text
				"setlong",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setlong
			},
			// yaw setpoint subtype
			{
				// ID
				0x1E,
				// Command text
				"setyaw",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setyaw
			},
			// pitch setpoint subtype
			{
				// ID
				0x1F,
				// Command text
				"setpitch",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setpitch
			},
			// roll setpoint subtype
			{
				// ID
				0x20,
				// Command text
				"setroll",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_setroll
			}
		}
	},

	// REQUEST
	{
		// Message Type ID
		0x02,

		// Calibration Subtypes (PID coefficients)
		{
			// yaw p constant subtype
			{
				// ID
				0x00,
				// Command text
				"getyawp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getyawp
			},
			// yaw i constant subtype
			{
				// ID
				0x01,
				// Command text
				"getyawi",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getyawi
			},
			// yaw d constant subtype
			{
				// ID
				0x02,
				// Command text
				"getyawd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getyawd
			},
			// roll p constant subtype
			{
				// ID
				0x03,
				// Command text
				"getrollp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getrollp
			},
			// roll i constant subtype
			{
				// ID
				0x04,
				// Command text
				"getrolli",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getrolli
			},
			// roll d constant subtype
			{
				// ID
				0x05,
				// Command text
				"getrolld",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getrolld
			},
			// pitch p constant subtype
			{
				// ID
				0x06,
				// Command text
				"getpitchp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getpitchp
			},
			// pitch i constant subtype
			{
				// ID
				0x07,
				// Command text
				"getpitchi",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getpitchi
			},
			// pitch d constant subtype
			{
				// ID
				0x08,
				// Command text
				"getpitchd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getpitchd
			},
			// yawrate p constant subtype
			{
				// ID
				0x09,
				// Command text
				"getyawratep",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getyawratep
			},
			// yawrate i constant subtype
			{
				// ID
				0x0A,
				// Command text
				"getyawratei",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getyawratei
			},
			// yawrate d constant subtype
			{
				// ID
				0x0B,
				// Command text
				"getyawrated",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getyawrated
			},
			// rollrate p constant subtype
			{
				// ID
				0x0C,
				// Command text
				"getrollratep",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getrollratep
			},
			// rollrate i constant subtype
			{
				// ID
				0x0D,
				// Command text
				"getrollratei",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getrollratei
			},
			// rollrate d constant subtype
			{
				// ID
				0x0E,
				// Command text
				"getrollrated",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getrollrated
			},
			// pitchrate p constant subtype
			{
				// ID
				0x0F,
				// Command text
				"getpitchratep",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getpitchratep
			},
			// pitchrate i constant subtype
			{
				// ID
				0x10,
				// Command text
				"getpitchratei",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getpitchratei
			},
			// pitchrate d constant subtype
			{
				// ID
				0x11,
				// Command text
				"getpitchrated",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getpitchrated
			},
			// height p constant subtype
			{
				// ID
				0x12,
				// Command text
				"getheightp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getheightp
			},
			// height i constant subtype
			{
				// ID
				0x13,
				// Command text
				"getheighti",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getheighti
			},
			// height d constant subtype
			{
				// ID
				0x14,
				// Command text
				"getheightd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getheightd
			},
			// lat p constant subtype
			{
				// ID
				0x15,
				// Command text
				"getlatp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getlatp
			},
			// lat i constant subtype
			{
				// ID
				0x16,
				// Command text
				"getlati",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getlati
			},
			// lat d constant subtype
			{
				// ID
				0x17,
				// Command text
				"getlatd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getlatd
			},
			// long p constant subtype
			{
				// ID
				0x18,
				// Command text
				"getlongp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getlongp
			},
			// long i constant subtype
			{
				// ID
				0x19,
				// Command text
				"getlongi",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getlongi
			},
			// long d constant subtype
			{
				// ID
				0x1A,
				// Command text
				"getlongd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getlongd
			},
			// height setpoint subtype
			{
				// ID
				0x1B,
				// Command text
				"getheight",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getheight
			},
			// lat getpoint subtype
			{
				// ID
				0x1C,
				// Command text
				"getlat",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getlat
			},
			// long getpoint subtype
			{
				// ID
				0x1D,
				// Command text
				"getlong",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getlong
			},
			// yaw getpoint subtype
			{
				// ID
				0x1E,
				// Command text
				"getyaw",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getyaw
			},
			// pitch getpoint subtype
			{
				// ID
				0x1F,
				// Command text
				"getpitch",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getpitch
			},
			// roll getpoint subtype
			{
				// ID
				0x20,
				// Command text
				"getroll",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_getroll
			}

		}
	},

	// RESPONSE
	{
		// Message Type ID
		0x03,

		// Calibration Subtypes (PID coefficients)
		{
			// yaw p constant subtype
			{
				// ID
				0x00,
				// Command text
				"respyawp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_respyawp
			},
			// yaw i constant subtype
			{
				// ID
				0x01,
				// Command text
				"respyawi",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_respyawi
			},
			// yaw d constant subtype
			{
				// ID
				0x02,
				// Command text
				"respyawd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_respyawd
			},
			// roll p constant subtype
			{
				// ID
				0x03,
				// Command text
				"resprollp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resprollp
			},
			// roll i constant subtype
			{
				// ID
				0x04,
				// Command text
				"resprolli",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resprolli
			},
			// roll d constant subtype
			{
				// ID
				0x05,
				// Command text
				"resprolld",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resprolld
			},
			// pitch p constant subtype
			{
				// ID
				0x06,
				// Command text
				"resppitchp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resppitchp
			},
			// pitch i constant subtype
			{
				// ID
				0x07,
				// Command text
				"resppitchi",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resppitchi
			},
			// pitch d constant subtype
			{
				// ID
				0x08,
				// Command text
				"resppitchd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resppitchd
			},
			// yawrate p constant subtype
			{
				// ID
				0x09,
				// Command text
				"respyawratep",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_respyawratep
			},
			// yawrate i constant subtype
			{
				// ID
				0x0A,
				// Command text
				"respyawratei",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_respyawratei
			},
			// yawrate d constant subtype
			{
				// ID
				0x0B,
				// Command text
				"respyawrated",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_respyawrated
			},
			// rollrate p constant subtype
			{
				// ID
				0x0C,
				// Command text
				"resprollratep",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resprollratep
			},
			// rollrate i constant subtype
			{
				// ID
				0x0D,
				// Command text
				"resprollratei",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resprollratei
			},
			// rollrate d constant subtype
			{
				// ID
				0x0E,
				// Command text
				"resprollrated",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resprollrated
			},
			// pitchrate p constant subtype
			{
				// ID
				0x0F,
				// Command text
				"resppitchratep",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resppitchratep
			},
			// pitchrate i constant subtype
			{
				// ID
				0x10,
				// Command text
				"resppitchratei",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resppitchratei
			},
			// pitchrate d constant subtype
			{
				// ID
				0x11,
				// Command text
				"resppitchrated",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resppitchrated
			},
			// height p constant subtype
			{
				// ID
				0x12,
				// Command text
				"respheightp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_respheightp
			},
			// height i constant subtype
			{
				// ID
				0x13,
				// Command text
				"respheighti",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_respheighti
			},
			// height d constant subtype
			{
				// ID
				0x14,
				// Command text
				"respheightd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_respheightd
			},
			// lat p constant subtype
			{
				// ID
				0x15,
				// Command text
				"resplatp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resplatp
			},
			// lat i constant subtype
			{
				// ID
				0x16,
				// Command text
				"resplati",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resplati
			},
			// lat d constant subtype
			{
				// ID
				0x17,
				// Command text
				"resplatd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resplatd
			},
			// long p constant subtype
			{
				// ID
				0x18,
				// Command text
				"resplongp",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resplongp
			},
			// long i constant subtype
			{
				// ID
				0x19,
				// Command text
				"resplongi",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resplongi
			},
			// long d constant subtype
			{
				// ID
				0x1A,
				// Command text
				"resplongd",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resplongd
			},
			// height setpoint subtype
			{
				// ID
				0x1B,
				// Command text
				"respheight",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_respheight
			},
			// lat resppoint subtype
			{
				// ID
				0x1C,
				// Command text
				"resplat",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resplat
			},
			// long resppoint subtype
			{
				// ID
				0x1D,
				// Command text
				"resplong",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resplong
			},
			// yaw resppoint subtype
			{
				// ID
				0x1E,
				// Command text
				"respyaw",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_respyaw
			},
			// pitch resppoint subtype
			{
				// ID
				0x1F,
				// Command text
				"resppitch",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resppitch
			},
			// roll resppoint subtype
			{
				// ID
				0x20,
				// Command text
				"resproll",
				// Type of the command data
				floatType,
				// Function pointer
				&cb_resproll
			}
		}
	},
	// UPDATE
	{
		// Message Type ID
		0x04,

		// Update Subtypes
		{
			// NONE subtype
			{
				// ID
				0x00,
				// Command text
				"update",
				// Type of the command data
				stringType,
				// Function pointer
				&cb_update
			},
			// BEGIN update
			{
				// ID
				0x01,
				// Command text
				"beginupdate",
				// Type of the command data
				stringType,
				// Function pointer
				&cb_beginupdate
			}
		}
	},

	// LOG
	{
		// Message Type ID
		0x05,

		// Log Subtypes
		{
			// NONE subtype
			{
				// ID
				0x00,
				// Command text
				"log",
				// Type of the command data
				stringType,
				// Function pointer
				&cb_log
			},
			// Response subtype
			{
				// ID
				0x01,
				// Command text
				"response",
				// Type of the command data
				stringType,
				// Function pointer
				&cb_response
			}
		}
	},

};
