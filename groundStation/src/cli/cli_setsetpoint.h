#ifndef __CLI_SETSETPOINT_H
#define __CLI_SETSETPOINT_H

#include "frontend_setsetpoint.h"

int cli_setsetpoint(struct backend_conn * conn, int argc, char ** argv);

#endif /* __CLI_SETSETPOINT_H */