#ifndef __CLI_H
#define __CLI_H

#include "cli_setsetpoint.h"
#include "cli_getsetpoint.h"
#include "cli_monitor.h"
#include "cli_setpid.h"
#include "cli_getpid.h"
#include "cli_getimu.h"

enum CommandNameIds{
	CMD_MONITOR,
	CMD_GETPID,
	CMD_SETPID,
	CMD_GETIMU,
	CMD_SETSETPOINT,
	CMD_GETSETPOINT,
	MAX_COMMANDS
};

typedef int (*cli_function_ptr)(struct backend_conn *, int, char **);
static cli_function_ptr cli_functions[] = {
	&cli_monitor,
	&cli_getpid,
	&cli_setpid,
	&cli_getimu,
	&cli_setsetpoint,
	&cli_getsetpoint
};

static char* commandNames[MAX_COMMANDS] = {
	"monitor",
	"getpid",
	"setpid",
	"getimu",
	"setsetpoint",
	"getsetpoint"
};

#endif /* __CLI_H */