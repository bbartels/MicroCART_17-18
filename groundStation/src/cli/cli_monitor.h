#ifndef __CLI_MONITOR_H
#define __CLI_MONITOR_H

#include <time.h>

#include "frontend_getpid.h"

#define SECOND_IN_NANO 1000000000

int cli_monitor(struct backend_conn * conn,	int argc, char **argv);

// Return 0 on success and 1 otherwise
int monitor(struct backend_conn * conn);
#endif /* __CLI_MONITOR_H */