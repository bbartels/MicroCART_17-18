#include <stdio.h>
#include <unistd.h>
#include <getopt.h>

#include "cli_setsetpoint.h"

int cli_setsetpoint(struct backend_conn * conn,	int argc, char **argv) {
	int c;
	static int setheight = 0, setlat = 0, setlong = 0;
	static int setpitch = 0, setroll = 0, setyaw = 0;
	struct frontend_setpoint_data setpoint_data;
	static int needHelp = 0;
	static int mask;
	static struct option long_options[] = {
 		/* These options don’t set a flag. We distinguish them by their indices. */
 		{"help",	no_argument,	    &needHelp,	1},
 		{"height",  required_argument,	0,  'h'},
 		{"long",	required_argument,	0,	'l'},
 		{"lat",		required_argument, 	0,	't'},
 		{"pitch",   required_argument,	0,  'p'},
 		{"roll",	required_argument,	0,	'r'},
 		{"yaw",		required_argument, 	0,	'y'},
 		{0, 0, 0, 0}
 	};

 	while (1)
	{
		/* getopt_long stores the option index here. */
		int option_index = 0;

		c = getopt_long(argc, argv, "", long_options, &option_index);

		/* Detect the end of the options. */
		if (c == -1)
			break;

		switch(c) {
			case 'h' :
				setpoint_data.height  = atof(optarg);
				mask |= SET_HEIGHT;
				break;
			case 't' :
				setpoint_data.lat  = atof(optarg);
				mask |= SET_LAT;
				break;
			case 'l' :
				setpoint_data.longg  = atof(optarg);
				mask |= SET_LONG;
				break;
			case 'p' :
				setpoint_data.pitch  = atof(optarg);
				mask |= SET_PITCH;
				break;
			case 'r' :
				setpoint_data.roll  = atof(optarg);
				mask |= SET_ROLL;
				break;
			case 'y' :
				setpoint_data.yaw  = atof(optarg);
				mask |= SET_YAW;
				break;
			default :
				break;
		}

	}

	if (needHelp) {
		printf("Setsetpoint sets the height, lat, long, pitch, roll, or yaw set point values for the quad.\n");
		printf("Usage Syntax : \n\t./Cli setsetpoint [options...]\n");
		printf("Symlink Usage Syntax : \n\t./setsetpoint [options...]\n\n");
		printf("Available options include the following\n");
		printf("\t[--height] 'val' : Sets the height setpoint value to 'val'\n");
		printf("\t[--lat]    'val' : Sets the latitude setpoint value to 'val'\n");
		printf("\t[--long]   'val' : Sets the longitude setpoint value to 'val'\n");
		printf("\t[--pitch]  'val' : Sets the pitch setpoint value to 'val'\n");
		printf("\t[--roll]   'val' : Sets the roll setpoint value to 'val'\n");
		printf("\t[--yaw]    'val' : Sets the yaw setpoint value to 'val'\n");
		return 0;
	}

	if (argc < 2) {
		printf("Incorrect Usage, run './cli setpid --help' for correct usage.\n");
		return 1;
	}
	
	frontend_setsetpoint(conn, &setpoint_data, mask);
	return 0;
}
