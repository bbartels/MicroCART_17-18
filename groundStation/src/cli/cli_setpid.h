#ifndef __CLI_SETPID_H
#define __CLI_SETPID_H

#include "frontend_setpid.h"

int cli_setpid(struct backend_conn * conn, int argc, char ** argv);

#endif /* __CLI_SETPID_H */