#ifndef __CLI_GETSETPOINT_H
#define __CLI_GETSETPOINT_H

#include "frontend_getsetpoint.h"

enum {
	HEIGHT,
	LAT,
	LONGG,
	PITCH,
	ROLL,
	YAW
};

int getSetPointValues(struct backend_conn *, struct frontend_setpoint_data *, int type);
int cli_getsetpoint(struct backend_conn * conn, int argc, char ** argv);

#endif /* __CLI_GETSETPOINT_H */