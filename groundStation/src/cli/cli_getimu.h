#ifndef __CLI_GETIMU_H
#define __CLI_GETIMU_H

#include "frontend_getimu.h"

int cli_getimu(struct backend_conn * conn, int argc, char ** argv);

#endif /* __CLI_GETIMU_H */