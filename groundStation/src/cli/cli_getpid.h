#ifndef __CLI_GETPID_H
#define __CLI_GETPID_H

#include "frontend_getpid.h"

int getPidValues(struct backend_conn *, struct frontend_pid_data *);
int cli_getpid(struct backend_conn * conn, int argc, char ** argv);

#endif /* __CLI_GETPID_H */