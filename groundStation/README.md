# groundStation

## Make Process

First, if submodules were not recursevly added through git. Run this command if you have made any attempt to make vrpn manually. 
run
	'git submodule update --init --recursive'

Now that you have all of the files necissary.

cd into the groundstation folder.
	cd groundStation
	make vrpn
	make

run the program with sudo privledges
	sudo -E ./BackEnd

If you wish to change the way the backend communicates to the quad and vice versa, look at src/config.h. 
	This provides a list of environment variables which you can set and use for your computer or time of use.
	Because the backend must be ran with sudo privledges, you will need to preserve the env. vars. with sudo rights.
	Hence the "-E" flag.

## Modifying
See MODIFYING for the software architecture/organization and how to add new functionality.

## Using
First, the backend daemon must be running. Run the backend with ./BackEnd. Note
the environment variables in config.h, especially the backend socket path. The backend
requires root for bluetooth, but can run as a normal user with TCP, as long as the
socket path is writable by the user.

Once the backend is running, various CLI tools can be used to view the state of the
quad and send commands. Each of these tools is given as the first argument
to the CLI program, for example, to monitor the quad, use `cli monitor`. Note that
the backend socket environment variable must be set to the same value as it
was for the backend. The CLI program also supports busybox-style symbolic links.
For example, if there is a symlink named `monitor` that points to the `cli` binary,
running the `monitor` program will effectively run `cli monitor`.

The names of the binaries is subject to change.

For more in depth usage explainations, use the --help flag.
'./Cli --help'

For help with the specific cli command you are running, use the --help flag once again.
'./Cli setpid --help'

### Example
In one terminal or screen, run the backend:

`UCART_SOCKET=./ucart.socket ./BackEnd`

This will activate the quad and the backend, and the backend will be available for
connections from the frontend tools. One useful tool is the monitor. In another
terminal window, run the monitor forever:

`UCART_SOCKET=./ucart.socket ./cli monitor -f`

This will begin a periodic monitoring that updates 10 times per second.

Finally, in a third window, export the socket path:

`export UCART_SOCKET=./ucart.socket`

and then run any other tools to modify the quad, for example modifying PID constants:

`./cli setpid --pitch -p 1.000`

