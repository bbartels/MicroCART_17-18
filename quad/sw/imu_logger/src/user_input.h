/*
 * user_input.h
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

#ifndef USER_INPUT_H_
#define USER_INPUT_H_
 
#include <stdio.h>
#include "type_def.h"
#include "log_data.h"
#include "util.h"
#include "stringBuilder.h"

/*
 * Aero channel declaration
 */

#define THROTTLE 0
#define ROLL     1
#define PITCH    2
#define YAW      3
#define GEAR 	 4
#define FLAP 	 5

//////TARGETS

#define YAW_DEG_TARGET 60.0f
#define YAW_RAD_TARGET ((float) ((YAW_DEG_TARGET * 3.141592) / ((float) 180)))

#define ROLL_DEG_TARGET 10.0f
#define ROLL_RAD_TARGET ((float) ((ROLL_DEG_TARGET * 3.141592) / ((float) 180)))

#define PITCH_DEG_TARGET 12.0f
#define PITCH_RAD_TARGET ((float) ((PITCH_DEG_TARGET * 3.141592) / ((float) 180)))

/////// Signals from the Rx mins, maxes and ranges

//#define THROTTLE_MAX  191900
//#define THROTTLE_MIN  110200
//#define THROTTLE_RANGE THROTTLE_MAX - THROTTLE_MIN
//
//#define ROLL_MAX      170200
//#define ROLL_MIN      129400
//#define ROLL_CENTER   149800
//#define ROLL_RANGE    ROLL_MAX - ROLL_MIN
//
//#define PITCH_MAX     169900
//#define PITCH_MIN     129500
//#define PITCH_CENTER  149700
//#define PITCH_RANGE   PITCH_MAX - PITCH_MIN
//
//#define YAW_MAX       169400
//#define YAW_MIN       129300
//#define YAW_CENTER   (YAW_MIN + YAW_MAX)/2 //149800
//#define YAW_RANGE     YAW_MAX - YAW_MIN
//
//#define GEAR_1	170800
//#define GEAR_0	118300
//
//#define FLAP_1		192000
//#define FLAP_0		107600
//
//#define GEAR_KILL     GEAR_0 // The kill point for the program
//#define BASE          150000

/**
 * @brief 
 *      Receives user input to the system.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param user_input_struct
 *      structure of the data inputed by the user
 *
 * @return 
 *      error message
 *
 */
int get_user_input(log_t* log_struct,  user_input_t* user_input_struct);
int kill_condition(user_input_t* user_input_struct);
float convert_from_receiver_cmd(int receiver_cmd, int max_receiver_cmd, int center_receiver_cmd, int min_receiver_cmd, float max_target, float min_target);


#endif /* USER_INPUT_H_ */
