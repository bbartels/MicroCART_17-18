/*
 * sensor_processing.h
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

#ifndef SENSOR_PROCESSING_H_
#define SENSOR_PROCESSING_H_
 
#include <stdio.h>
#include "log_data.h"
#include "sensor.h"
#include "conversion.h"
#include "quadposition.h"
/*
 * Aero channel declaration
 */

#define THROTTLE 0
#define ROLL     1
#define PITCH    2
#define YAW      3
#define GEAR 	 4
#define FLAP 	 5

/**
 * Signals from the Rx mins, maxes and ranges
 */
//#define THROTTLE_MAX  191900
//#define THROTTLE_MIN  110200
//#define THROTTLE_RANGE THROTTLE_MAX - THROTTLE_MIN
//
//#define ROLL_MAX      170200
////#define ROLL_MAX      167664
////#define ROLL_CENTER   148532
//#define ROLL_MIN      129400
//#define ROLL_CENTER   149800
//#define ROLL_RANGE    ROLL_MAX - ROLL_MIN
//
////#define PITCH_MAX     190800
//#define PITCH_MAX     169900
////#define PITCH_MIN     135760
//#define PITCH_MIN     129500
////#define PITCH_CENTER  152830
//#define PITCH_CENTER  149700
//#define PITCH_RANGE   PITCH_MAX - PITCH_MIN
//
//#define YAW_MAX       169400
//#define YAW_MIN       129300
//#define YAW_CENTER   149800
//#define YAW_RANGE     YAW_MAX - YAW_MIN
//
//#define GEAR_1	170800
//#define GEAR_0	118300
//
//#define FLAP_1		192000
//#define FLAP_0		107600
//
//#define GEAR_KILL     GEAR_0 // The kill point for the program
//#define BASE          150000
//
//#define min 100000
//#define max 200000

/**
 * @brief 
 *      Processes the data from the sensors.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @param raw_sensor_struct
 *      structure of the raw data from the sensors
 *
 * @param sensor_struct
 *      structure of the processed data from the sensors
 *
 * @return 
 *      error message
 *
 */

int sensor_processing(log_t* log_struct, user_input_t *user_input_struct, raw_sensor_t * raw_sensor_struct, sensor_t* sensor_struct);
void deep_copy_Qpos(quadPosition_t * dest, quadPosition_t * src);
void set_pitch_angle_filtered(sensor_t * sensor_struct, float accel_roll);
void set_roll_angle_filtered(sensor_t * sensor_struct, float accel_roll);

#endif /* SENSOR_PROCESSING_H_ */
