/*
 * initialize_components.h
 *
 *  Created on: Nov 12, 2015
 *      Author: ucart
 */

#ifndef INITALIZE_COMPONENTS_H_
#define INITALIZE_COMPONENTS_H_

#include "timer.h"
#include "control_algorithm.h"
#include "platform.h"
#include "uart.h"
#include "iic_mpu9150_utils.h"
#include "util.h"
#include "controllers.h"

/**
 * @brief 
 *      Runs loops to make sure the quad is responding and in the correct state before starting.
 *
 * @return
 *      error message
 *
 */
int protection_loops();

/**
 * @brief
 *      Initializes the sensors, communication, and anything else that needs
 * initialization.
 *
 * @return 
 *      error message
 *
 */
int initializeAllComponents(user_input_t * user_input_struct, log_t * log_struct, raw_sensor_t * raw_sensor_struct,
		sensor_t * sensor_struct, setpoint_t * setpoint_struct, parameter_t * parameter_struct, user_defined_t *user_defined_struct,
		raw_actuator_t * raw_actuator_struct, actuator_command_t * actuator_command_struct);

#endif /* INITALIZE_COMPONENTS_H_ */

