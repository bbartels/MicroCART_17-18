/*
 * main.c
 *
 *  Created on: Nov 11, 2015
 *      Author: ucart
 */
#include <stdio.h>
#include "timer.h"
#include "log_data.h"
#include "initialize_components.h"
#include "user_input.h"
#include "sensor.h"
#include "sensor_processing.h"
#include "control_algorithm.h"
#include "actuator_command_processing.h"
#include "send_actuator_commands.h"
#include "update_gui.h"

int main()
{
	// Structures to be used throughout
	modular_structs_t structs = { };


	// Initialize all required components and structs:
	// Uart, PWM receiver/generator, I2C, Sensor Board
	// Xilinx Platform, Loop Timer, Control Algorithm
	int init_error = initializeAllComponents(&(structs.user_input_struct), &(structs.log_struct),
			&(structs.raw_sensor_struct), &(structs.sensor_struct), &(structs.setpoint_struct), &(structs.parameter_struct),
			&(structs.user_defined_struct), &(structs.raw_actuator_struct), &(structs.actuator_command_struct));

	if (init_error != 0) {
		printf("ERROR (main): Problem initializing...Goodbye\r\n");
		return -1;
	}

	// Loops to make sure the quad is responding correctly before starting the control loop
	//protection_loops();

	printf("The quad loop is now beginning.\n");

	char send_str[512];
	double timestamp = 0;
	// Main control loop
	do
	{
		// Processing of loop timer at the beginning of the control loop
		timer_start_loop();

		// Get data from the sensors and put it into raw_sensor_struct
		get_sensors(&(structs.log_struct), &(structs.user_input_struct), &(structs.raw_sensor_struct));


		sprintf(send_str, "%f,%f,%f,%f,%f,%f,%f\n", timestamp,
				structs.raw_sensor_struct.gam.accel_x,
				structs.raw_sensor_struct.gam.accel_y,
				structs.raw_sensor_struct.gam.accel_z,
				structs.raw_sensor_struct.gam.gyro_xVel_p,
				structs.raw_sensor_struct.gam.gyro_yVel_q,
				structs.raw_sensor_struct.gam.gyro_zVel_r);

		uart0_sendStr(send_str);

		// Process the sensor data and put it into sensor_struct
		//sensor_processing(&(structs.log_struct), &(structs.user_input_struct), &(structs.raw_sensor_struct), &(structs.sensor_struct));


		//if(structs.user_defined_struct.flight_mode == MANUAL_FLIGHT_MODE)
			//MIO7_led_on();

		// Processing of loop timer at the end of the control loop
		timestamp = timer_end_loop(&(structs.log_struct));

	} while(!kill_condition(&(structs.user_input_struct)));


	stringBuilder_free((structs.user_input_struct).sb);

	pwm_kill();

	MIO7_led_off();

	printLogging();

	flash_MIO_7_led(10, 100);

	return 0;
}


