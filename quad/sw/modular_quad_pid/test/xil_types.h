#ifndef XIL_TYPES_H
#define XIL_TYPES_H

#include <stddef.h>

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

#endif
