#include <stdio.h>
#include "uart_buff.h"
#include <math.h>
#include <string.h>

int main() {
  int failed = 0;
  failed += test_empty_when_empty();
  failed += test_empty_after_receiving_some_data();
  failed += test_full_is_false_at_start();
  failed += test_full_after_receiving_some_data();
  failed += test_packet_get_u8();
  failed += test_packet_get_u8_with_offset();
  failed += test_packet_get_u16();
  failed += test_packet_get_u16_with_offset();
  failed += test_packet_get_u16_wrapped();
  failed += test_packet_get_u32();
  failed += test_packet_get_u32_with_offset();
  failed += test_packet_get_u32_wrapped_1_4();
  failed += test_packet_get_u32_wrapped_2_4();
  failed += test_packet_get_u32_wrapped_3_4();
  failed += test_packet_get_float();
  failed += test_packet_get_float_with_offset();
  failed += test_buffer_size_after_VRPN_packet();
  failed += test_buffer_size_empty();
  failed += test_buffer_size_after_garbage_data();
  failed += test_buffer_size_after_garbage_data_scanned();
  failed += test_packet_ready_at_start();
  failed += test_packet_ready_after_receiving_packet();
  failed += test_packet_ready_after_consuming_packet();
  failed += test_size_when_data_lenth_too_large();
  failed += test_get_raw();

  printf("Total tests failed: %d\n", failed);

  return 0;
}

int float_equals(float x1, float x2) {
  return fabs(x1 - x2) < 10e-5;
}

void print_test_result(int success, float exp, float act) {
  if (success) printf("passed\n");
  else printf("FAILED: expected %f but got %f\n", exp, act);
}

int failed(char *msg) {
  printf("%s\n", msg);
  return 1;
}

void add_packet(unsigned char type, unsigned char subtype, unsigned short id, unsigned short length, unsigned char *data) {
  uart_buff_add_u8(0xBE);
  uart_buff_add_u8(type);
  uart_buff_add_u8(subtype);
  uart_buff_add_u8(id);
  uart_buff_add_u8(id >> 8);
  uart_buff_add_u8(length);
  uart_buff_add_u8(length >> 8);
  int i;
  for (i = 0; i < length; i += 1) {
    uart_buff_add_u8(data[i]);
  }
  // fake checksum
  uart_buff_add_u8(1);
}

void add_VRPN_packet() {
  float arr[6] = {1.0, 1.2, 1.4, -1.5, -0.5, -1.1};
  unsigned char *data = (unsigned char *) &arr;
  add_packet(4, 0, 0, 24, data);
}

void add_basic_packet() {
  unsigned char data[6] = {0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF};
  add_packet(4, 0, 0, 6, data);
}

void add_garbage_data() {
  int i;
  for (i = 0; i < 32; i += 1) {
    uart_buff_add_u8(i);
  }
}


int setup_VRPN_packet() {
  add_VRPN_packet();
  if (!uart_buff_packet_ready()) {
    return 0;
  }
}

int setup_basic_packet() {
  add_basic_packet();
  if (!uart_buff_packet_ready()) {
    return 0;
  }
}

int test_empty_when_empty() {
  uart_buff_reset();
  int exp = 1;
  int act = uart_buff_empty();
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_empty_after_receiving_some_data() {
  uart_buff_reset();
  add_garbage_data();
  int exp = 0;
  int act = uart_buff_empty();
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_full_is_false_at_start() {
  uart_buff_reset();
  int exp = 0;
  int act = uart_buff_full();
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_full_after_receiving_some_data() {
  uart_buff_reset();
  add_garbage_data();
  int exp = 0;
  int act = uart_buff_full();
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_buffer_size_empty() {
  uart_buff_reset();
  int exp = 0;
  int act = uart_buff_size();
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_buffer_size_after_garbage_data() {
  uart_buff_reset();
  add_garbage_data();
  int exp = 32;
  int act = uart_buff_size();
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_buffer_size_after_garbage_data_scanned() {
  uart_buff_reset();
  add_garbage_data();
  uart_buff_packet_ready();
  int exp = 0;
  int act = uart_buff_size();
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_buffer_size_after_VRPN_packet() {
  uart_buff_reset();
  if(!setup_VRPN_packet()) return failed("FAILED: setup failed");
  int exp = 32;
  int act = uart_buff_size();
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_packet_get_u8() {
  uart_buff_reset();
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  int exp = 0xAA;
  int act = uart_buff_data_get_u8(0);
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_packet_get_u8_with_offset() {
  uart_buff_reset();
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  int exp = 0xBB;
  int act = uart_buff_data_get_u8(1);
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_packet_get_u16() {
  uart_buff_reset();
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  int exp = 0xBBAA;
  int act = uart_buff_data_get_u16(0);
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_packet_get_u16_with_offset() {
  uart_buff_reset();
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  int exp = 0xDDCC;
  int act = uart_buff_data_get_u16(2);
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_packet_get_u16_wrapped() {
  uart_buff_reset();
  int i;
  for (i = 0; i < 1000; i += 1) uart_buff_add_u8(0);
  uart_buff_packet_ready();
  for (i = 0; i < 1040; i += 1) uart_buff_add_u8(0);
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  int exp = 0xBBAA;
  int act = uart_buff_data_get_u16(0);
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_packet_get_u32() {
  uart_buff_reset();
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  unsigned int exp = 0xDDCCBBAA;
  unsigned int act = uart_buff_data_get_u32(0);
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_packet_get_u32_with_offset() {
  uart_buff_reset();
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  unsigned int exp = 0xFFEEDDCC;
  unsigned int act = uart_buff_data_get_u32(2);
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_packet_get_u32_wrapped_1_4() {
  uart_buff_reset();
  int i;
  for (i = 0; i < 1000; i += 1) uart_buff_add_u8(0);
  uart_buff_packet_ready();
  for (i = 0; i < 1040; i += 1) uart_buff_add_u8(0);
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  int exp = 0xDDCCBBAA;
  int act = uart_buff_data_get_u32(0);
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_packet_get_u32_wrapped_2_4() {
  uart_buff_reset();
  int i;
  for (i = 0; i < 1000; i += 1) uart_buff_add_u8(0);
  uart_buff_packet_ready();
  for (i = 0; i < 1039; i += 1) uart_buff_add_u8(0);
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  int exp = 0xDDCCBBAA;
  int act = uart_buff_data_get_u32(0);
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}

int test_packet_get_u32_wrapped_3_4() {
  uart_buff_reset();
  int i;
  for (i = 0; i < 1000; i += 1) uart_buff_add_u8(0);
  uart_buff_packet_ready();
  for (i = 0; i < 1038; i += 1) uart_buff_add_u8(0);
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  int exp = 0xDDCCBBAA;
  int act = uart_buff_data_get_u32(0);
  int success = exp == act;
  print_test_result(success, (float) exp, (float) act);
  return !success;
}


int test_packet_get_float() {
  uart_buff_reset();
  if(!setup_VRPN_packet()) return failed("FAILED: setup failed");
  float exp = 1.0;
  float act = uart_buff_data_get_float(0);
  int success = float_equals(exp, act);
  print_test_result(success, exp, act);
  return !success;
}

int test_packet_get_float_with_offset() {
  uart_buff_reset();
  if(!setup_VRPN_packet()) return failed("FAILED: setup failed");
  float exp = 1.2;
  float act = uart_buff_data_get_float(4);
  int success = float_equals(exp, act);
  print_test_result(success, exp, act);
  return !success;
}

int test_packet_ready_at_start() {
  uart_buff_reset();
  int exp = 0;
  int act = uart_buff_packet_ready();
  int success = act == exp;
  print_test_result(success, exp, act);
  return !success;
}

int test_packet_ready_after_receiving_packet() {
  uart_buff_reset();
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  int exp = 1;
  int act = uart_buff_packet_ready();
  int success = act == exp;
  print_test_result(success, exp, act);
  return !success;
}

int test_packet_ready_after_consuming_packet() {
  uart_buff_reset();
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  uart_buff_consume_packet();
  int exp = 0;
  int act = uart_buff_packet_ready();
  int success = act == exp;
  print_test_result(success, exp, act);
  return !success;
}

int test_size_when_data_lenth_too_large() {
  uart_buff_reset();
  unsigned char data[UART_MAX_PACKET_SIZE + 1];
  add_packet(4, 0, 0, UART_MAX_PACKET_SIZE + 1, data);
  uart_buff_packet_ready();
  int exp = 0;
  int act = uart_buff_size();
  int success = act == exp;
  print_test_result(success, exp, act);
  return !success;
}

int test_get_raw() {
  uart_buff_reset();
  if(!setup_basic_packet()) return failed("FAILED: setup failed");
  unsigned char exp[15] =
    {0xBE, 0x04, 0x00, 0x00, 0x00, 0x06, 0x00, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x01};
  size_t length;
  unsigned char *act = (unsigned char *) uart_buff_get_raw(&length);
  int success = 1;
  int i;
  for (i = 0; i < length; i += 1) {
    success = success && (exp[i] == act[i]);
    if (!success) {
      printf("FAILED");
      break;
    }
  }
  if (success) printf("passed\n");
  return !success;
}
