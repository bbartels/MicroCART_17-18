/*
 * process_packet.c
 *
 *  Created on: Mar 2, 2016
 *      Author: ucart
 */
#include "packet_processing.h"
#include "uart.h"
#include "type_def.h"
#include "sleep.h"
#include "util.h"
#include "communication.h"

#define DEBUG 0

tokenList_t tokenize(char* cmd) {
	int maxTokens = 16;
	tokenList_t ret;
	ret.numTokens = 0;
	ret.tokens = malloc(sizeof(char *)* 20 * maxTokens);

	int i = 0;
	ret.tokens[0] = NULL;
	char* token = strtok(cmd, " ");
	while (token != NULL && i < maxTokens - 1) {
		ret.tokens[i] = malloc(strlen(token) + 10);
		strcpy(ret.tokens[i], token);
		ret.tokens[++i] = NULL;
		ret.numTokens++;
		token = strtok(NULL, " ");
	}

	return ret;
}

float getFloat(unsigned char* str, int pos) {
	union {
		float f;
		int i;
	} x;
	x.i = ((str[pos+3] << 24) | (str[pos+2] << 16) | (str[pos+1] << 8) | (str[pos]));
	return x.f;
}

int getInt(unsigned char* str, int pos) {
	int i = ((str[pos+3] << 24) | (str[pos+2] << 16) | (str[pos+1] << 8) | (str[pos]));
	return i;
}
