#include "commands.h"

/* The cb_default used on the groundStation. This file MUST NOT BE INCLUDED
 * by anything except for commands.c */

/* cb_default used by portable commands.c */
int cb_default(modular_structs_t *structs)
{
	    return 0;
}
