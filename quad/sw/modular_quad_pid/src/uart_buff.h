#ifndef CIRC_BUFF_H
#define CIRC_BUFF_H

#include "xil_types.h"

#define UART_MAX_BUFF_SIZE 2048
#define UART_MAX_PACKET_SIZE 256
#define UART_PACKET_HEADER_SIZE 7

void uart_buff_add_u8(u8);
int uart_buff_packet_ready();
u8 uart_buff_get_u8(size_t);
u16 uart_buff_get_u16(size_t);
u32 uart_buff_get_u32(size_t);
u8 uart_buff_data_get_u8(size_t);
u16 uart_buff_data_get_u16(size_t);
u32 uart_buff_data_get_u32(size_t);
float uart_buff_data_get_float(size_t);
void uart_buff_consume_packet();
size_t uart_buff_size();
size_t uart_buff_data_length();
int uart_buff_empty();
int uart_buff_full();
char * uart_buff_get_raw(size_t *);
void uart_buff_print();
u32 uart_buff_packets_processed();
void uart_buff_reset();

#endif
