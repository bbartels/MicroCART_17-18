/*
 * mio7_led.h
 *
 *  Created on: Feb 20, 2016
 *      Author: Amy Seibert
 */

#ifndef MIO7_LED_H_
#define MIO7_LED_H_
 
#include <stdio.h>
#include <xgpiops.h>
#include "sleep.h"

/**
 * @brief 
 *      Flashes the MIO7 LED how_many_times times and with ms_between_flashes between the flashes. 
 *
 * @param how_many_times
 *      times the LED should be flashed
 *
 * @param ms_between_flashes
 *      time between flashes in milliseconds
 *
 */
void flash_MIO_7_led(int how_many_times, int ms_between_flashes);

/**
 * @brief 
 *      Turns off MIO7 LED. 
 *
 */
void MIO7_led_off();

/**
 * @brief 
 *      Turns on MIO7 LED. 
 *
 */
void MIO7_led_on();

#endif /* MIO7_LED_H_ */
