#ifndef _COMMUNICATION_H
#define _COMMUNICATION_H

#include <xuartps.h>
#include "xparameters.h"
#include "xscugic.h"
#include "xil_exception.h"

#include "type_def.h"
#include "uart.h"
#include "mio7_led.h"
#include "timer.h"
#include "commands.h"
#include "uart_buff.h"

int initUartComms();
void process_received(modular_structs_t *structs);
void uart_interrupt_handler(XUartPs *InstancePtr);
int send_data(u16 type_id, u16 subtype_id, u16 msg_id, char* data, size_t size);

#endif
