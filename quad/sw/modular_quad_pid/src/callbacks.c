#include "communication.h"
#include "commands.h"
#include "type_def.h"
#include "uart.h"

int debug(modular_structs_t *structs)
{
	return 0;
}

static int n_msg_received = 0;
static size_t total_payload_received = 0;

int cb_packetlog(modular_structs_t* structs) {
	n_msg_received += 1;
	total_payload_received += uart_buff_data_length();
	return 0;
}

int cb_getpacketlogs(modular_structs_t* structs) {
	char buf[255];

	// Message logging number of messages received and size of payload received
	int length = snprintf(buf, sizeof buf, "%d,%d", n_msg_received, total_payload_received);

	send_data(MessageTypes[5].ID, MessageTypes[5].subtypes[1].ID, 0, buf, length >= sizeof(buf) ? 255 : length + 1);
	return 0;
}

/* Handles receiving new location updates */
int cb_update(modular_structs_t *structs)
{
	//processUpdate(packet, &(structs->raw_sensor_struct.currentQuadPosition));

	quadPosition_t* currentQuadPosition = &(structs->raw_sensor_struct.currentQuadPosition);
	// Packet must come as [NEARPY], 4 bytes each
	int packetId = uart_buff_data_get_u32(0);
//	printf("Packet ID: %d\n", packetId);
	float y_pos = uart_buff_data_get_float(4);
//	printf("y_pos: %f\n", y_pos);
	float x_pos = uart_buff_data_get_float(8);
//	printf("x_pos: %f\n", x_pos);
	float alt_pos = uart_buff_data_get_float(12);
//	printf("alt_pos: %f\n", alt_pos);
	float roll = uart_buff_data_get_float(16);
//	printf("roll: %f\n", roll);
	float pitch = uart_buff_data_get_float(20);
//	printf("pitch: %f\n", pitch);
	float yaw = uart_buff_data_get_float(24);
//	printf("yaw: %f\n", yaw);

	currentQuadPosition->packetId = packetId;
	currentQuadPosition->y_pos = y_pos;
	currentQuadPosition->x_pos = x_pos;
	currentQuadPosition->alt_pos = alt_pos;
	currentQuadPosition->roll = roll;
	currentQuadPosition->pitch = pitch;
	currentQuadPosition->yaw = yaw;

	// Make location as fresh
	structs->user_input_struct.locationFresh = 1;

	return 0;
}

// This is called on the ground station to begin sending VRPN to the quad
int cb_beginupdate(modular_structs_t *structs) {
	structs->user_input_struct.receivedBeginUpdate = 1;
	return 0;
}

int cb_log(modular_structs_t *structs)
{
	size_t length;
	unsigned char *packet = uart_buff_get_raw(&length);
	return 0;
}

int cb_response(modular_structs_t *structs)
{
	size_t length;
	char *packet = uart_buff_get_raw(&length);
	return 0;
}

int cb_setyaw(modular_structs_t *structs)
{
	structs->setpoint_struct.desiredQuadPosition.yaw = uart_buff_data_get_float(0);
	return 0;
}

int cb_setyawp(modular_structs_t *structs)
{
	structs->parameter_struct.yaw_angle_pid.Kp = uart_buff_data_get_float(0);
	return 0;
}

int cb_setyawd(modular_structs_t *structs)
{
	structs->parameter_struct.yaw_angle_pid.Kd = uart_buff_data_get_float(0);
	return 0;
}

int cb_setroll(modular_structs_t *structs)
{
	structs->setpoint_struct.desiredQuadPosition.roll = uart_buff_data_get_float(0);
	return 0;
}

int cb_setrollp(modular_structs_t *structs)
{
	structs->parameter_struct.roll_angle_pid.Kp = uart_buff_data_get_float(0);
	return 0;
}

int cb_setrolld(modular_structs_t *structs)
{
	structs->parameter_struct.roll_angle_pid.Kd = uart_buff_data_get_float(0);
	return 0;
}

int cb_setpitch(modular_structs_t *structs)
{
	structs->setpoint_struct.desiredQuadPosition.pitch = uart_buff_data_get_float(0);
	return 0;
}

int cb_setpitchp(modular_structs_t *structs)
{
	structs->parameter_struct.pitch_angle_pid.Kp = uart_buff_data_get_float(0);
	return 0;
}

int cb_setpitchd(modular_structs_t *structs)
{
	structs->parameter_struct.pitch_angle_pid.Kd = uart_buff_data_get_float(0);
	return 0;
}



// rate functions to make Andy happy

int cb_setyawratep(modular_structs_t *structs)
{
	structs->parameter_struct.yaw_ang_vel_pid.Kp = uart_buff_data_get_float(0);
	return 0;
}

int cb_setyawrated(modular_structs_t *structs)
{
	structs->parameter_struct.yaw_ang_vel_pid.Kd = uart_buff_data_get_float(0);
	return 0;
}

int cb_setrollratep(modular_structs_t *structs)
{
	structs->parameter_struct.roll_ang_vel_pid.Kp = uart_buff_data_get_float(0);
	return 0;
}

int cb_setrollrated(modular_structs_t *structs)
{
	structs->parameter_struct.roll_ang_vel_pid.Kd = uart_buff_data_get_float(0);
	return 0;
}

int cb_setpitchratep(modular_structs_t *structs)
{
	structs->parameter_struct.pitch_ang_vel_pid.Kp = uart_buff_data_get_float(0);
	return 0;
}

int cb_setpitchrated(modular_structs_t *structs)
{
	structs->parameter_struct.pitch_ang_vel_pid.Kd = uart_buff_data_get_float(0);
	return 0;
}





int cb_setheight(modular_structs_t *structs)
{
	structs->setpoint_struct.desiredQuadPosition.alt_pos = uart_buff_data_get_float(0);
	return 0;
}

int cb_setheightp(modular_structs_t *structs)
{
	structs->parameter_struct.alt_pid.Kp = uart_buff_data_get_float(0);
}

int cb_setheighti(modular_structs_t *structs)
{
	structs->parameter_struct.alt_pid.Ki = uart_buff_data_get_float(0);
}

int cb_setheightd(modular_structs_t *structs)
{
	structs->parameter_struct.alt_pid.Kd = uart_buff_data_get_float(0);
}
