#ifndef __callbacks_
#define __callbacks_h

/* Grab some stupid stuff from legacy code */
#include "type_def.h"

/* Make commands.c happy */
typedef int (command_cb)(modular_structs_t *structs);

#endif
