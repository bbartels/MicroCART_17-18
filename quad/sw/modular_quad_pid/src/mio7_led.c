/*
 * mio7_led.c
 *
 *  Created on: Feb 20, 2016
 *      Author: Amy Seibert
 */
 
 #include "mio7_led.h"
 
void flash_MIO_7_led(int how_many_times, int ms_between_flashes)
{
	if(how_many_times <= 0)
		return;

	while(how_many_times)
	{
		MIO7_led_on();

		usleep(ms_between_flashes * 500);

		MIO7_led_off();

		usleep(ms_between_flashes * 500);

		how_many_times--;
	}
}

void MIO7_led_off()
{
	XGpioPs Gpio;

	XGpioPs_Config * ConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	XGpioPs_CfgInitialize(&Gpio, ConfigPtr, ConfigPtr->BaseAddr);

	XGpioPs_SetDirectionPin(&Gpio, 7, 1);

	// Disable LED
	XGpioPs_WritePin(&Gpio, 7, 0x00);
}

void MIO7_led_on()
{
	XGpioPs Gpio;

	XGpioPs_Config * ConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	XGpioPs_CfgInitialize(&Gpio, ConfigPtr, ConfigPtr->BaseAddr);

	XGpioPs_SetDirectionPin(&Gpio, 7, 1);

	// Enable LED
	XGpioPs_WritePin(&Gpio, 7, 0x01);
}
