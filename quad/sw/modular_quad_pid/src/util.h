/*
 * util.h
 *
 *  Created on: Oct 11, 2014
 *      Author: ucart
 */
#ifndef _UTIL_H
#define _UTIL_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <xgpiops.h>
#include "PID.h"
#include "log_data.h"
#include <sleep.h>
#include "controllers.h"
#include "xparameters.h"
#include "uart.h"


#define clock_rate          100000000
#define frequency           450
#define period_width        clock_rate/frequency
#define pulse_throttle_low  clock_rate / 1000 // 1ms
#define pulse_throttle_high clock_rate / 500 // 2ms
#define MOTOR_0_PERCENT     115000


#define XPAR_BTNS_BASEADDR 0x41200000

/**
 * Various Addresses of custom IP components
 */
#define PWM_0_ADDR     XPAR_PWM_SIGNAL_OUT_WKILLSWITCH_0_BASEADDR
#define PWM_1_ADDR     XPAR_PWM_SIGNAL_OUT_WKILLSWITCH_1_BASEADDR
#define PWM_2_ADDR     XPAR_PWM_SIGNAL_OUT_WKILLSWITCH_2_BASEADDR
#define PWM_3_ADDR     XPAR_PWM_SIGNAL_OUT_WKILLSWITCH_3_BASEADDR
#define PWM_REC_0_ADDR XPAR_PWM_RECORDER_0_BASEADDR
#define PWM_REC_1_ADDR XPAR_PWM_RECORDER_1_BASEADDR
#define PWM_REC_2_ADDR XPAR_PWM_RECORDER_2_BASEADDR
#define PWM_REC_3_ADDR XPAR_PWM_RECORDER_3_BASEADDR
#define PWM_REC_4_ADDR XPAR_PWM_RECORDER_4_BASEADDR
#define PWM_REC_5_ADDR XPAR_PWM_RECORDER_5_BASEADDR

/**
 * Register offsets within the custom IP
 */
#define PWM_PERIOD     0
#define PWM_PULSE      4

void pwm_init();
void pwm_write_all(int pulseWidth);
void pwm_write_channel(int pulseWidth, int channel);

int read_rec(int channel);
void read_rec_all(int* mixer);

void read_bluetooth_all(int* mixer);

void b_drive_pulse();

void sine_example();

void print_mixer(int* mixer);
int read_kill(int gear);
int read_flap(int flap);
void pwm_kill();

void printLogging();
void bluetoothTunePID(char instr, gam_t* gam, PID_t* CFpid, PID_t* Gpid);
void msleep(int msecs);

/**
 * @brief
 *      Flashes the MIO7 LED how_many_times times and with ms_between_flashes between the flashes.
 *
 * @param how_many_times
 *      times the LED should be flashed
 *
 * @param ms_between_flashes
 *      time between flashes in milliseconds
 *
 */
void flash_MIO_7_led(int how_many_times, int ms_between_flashes);

/**
 * @brief
 *      Turns off MIO7 LED.
 *
 */
void MIO7_led_off();

/**
 * @brief
 *      Turns on MIO7 LED.
 *
 */
void MIO7_led_on();

#endif //_UTIL_H
