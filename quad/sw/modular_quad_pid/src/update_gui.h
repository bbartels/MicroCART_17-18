/*
 * update_gui.h
 *
 *  Created on: Feb 20, 2016
 *      Author: ucart
 */

#ifndef UPDATE_GUI_H_
#define UPDATE_GUI_H_
 
#include <stdio.h>

#include "log_data.h"

/**
 * @brief 
 *      Updates the user interface.
 *
 * @param log_struct
 *      structure of the data to be logged
 *
 * @return 
 *      error message
 *
 */
int update_GUI(log_t* log_struct);

#endif /* UPDATE_GUI_H_ */
