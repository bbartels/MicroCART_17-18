#!/usr/local/bin/python3.6

import sys
import time

import serial

def create_msg(main_type, subtype, msg_id, data):
    msg = bytes()
    msg += b'\xBE'
    msg += main_type.to_bytes(1, 'little')
    msg += subtype.to_bytes(1, 'little')
    msg += msg_id.to_bytes(2, 'little')
    msg += len(data).to_bytes(2, 'little')
    msg += data
    
    checksum = 0
    for b in msg:
        checksum ^= b
    msg += checksum.to_bytes(1, 'little')
    return msg

def create_test_packet(size=8):
    data = bytes((i % 256 for i in range(size)))
    return create_msg(0, 2, 0, data)

if __name__ == '__main__':
    with serial.Serial('/dev/ttyUSB0', 921600, timeout=5) as ser:
        for i in range(5):
            ser.reset_input_buffer()
            data = bytes.fromhex('be040002001c0002000000d80471be5732703f9d16093f8bf7a03d0586ab3d006d3a40c1')
            #ser.write(create_test_packet(24))
            ser.write(data)
            #ser.flush()
            #time.sleep(0.5)
