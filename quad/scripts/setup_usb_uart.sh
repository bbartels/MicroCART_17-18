#!/bin/bash

# The USB device that you are using for UART
USB_DEV=ttyUSB0

stty -F /dev/$USB_DEV raw speed 115200
