#!/usr/bin/python

import sys

print(sys.version_info)
import serial

if __name__ == '__main__':
    data = bytes.fromhex('be040002001c0002000000d80471be5732703f9d16093f8bf7a03d0586ab3d006d3a40c1')

    with serial.Serial('/dev/ttyUSB0', 921600) as ser:
        ser.write(data)
