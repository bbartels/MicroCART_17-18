#!/usr/local/bin/python3.6

import sys
import time

import serial

def read_packet(ser):
    header = ser.read(7)
    length = int.from_bytes(header[5:7], byteorder='little')
    data = ser.read(length)
    checksum = ser.read()
    return data

if __name__ == '__main__':
    with serial.Serial('/dev/ttyUSB0', 921600, timeout=5) as ser:
        i = 0
        while True:
            ser.reset_input_buffer()
            time.sleep(0.05)
            while ser.in_waiting != 0:
                resp = read_packet(ser)
                elapsed = int.from_bytes(resp[0:3], byteorder='little')
                processed = int.from_bytes(resp[4:7], byteorder='little')
                print("{} {} {}".format(i, elapsed, processed))
                i += 1
            ser.flush()

